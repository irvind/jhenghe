﻿using UnityEngine;
using System.Collections;

public class CharacterScreen : MonoBehaviour 
{
	public float waitTime = 3f;

	private ScreenChanger screenChanger;
	private float time;

	private bool trig = false;
	
	void Start() 
	{
		screenChanger = GameObject.FindGameObjectWithTag("ScreenChanger").GetComponent<ScreenChanger>();

		time = 0;
	}

	void Update() 
	{
		time += Time.deltaTime;
		
		if ((time > waitTime) && (trig == false))
		{
			screenChanger.GoToScreen(1);
			trig = true;
		}
	}
}
