﻿using UnityEngine;
using System.Collections;

public class TroiLogo : MonoBehaviour 
{
	public float waitTime = 3f;

	private ScreenChanger screenChanger;
	private float time;
	
	private bool trig = false;

	void Start() 
	{
		screenChanger = GameObject.FindGameObjectWithTag("ScreenChanger").GetComponent<ScreenChanger>();
		
		time = 0;

		GameObject managersObj = GameObject.Find("managers");
		DontDestroyOnLoad(managersObj);
		
		//Debug.Log("Points pref: " + PlayerPrefs.GetInt("Points").ToString());
	}

	void Update() 
	{
		time += Time.deltaTime;

		if ((time > waitTime) && (trig == false))
		{
			screenChanger.GoToScreen(4);
			trig = true;
		}
	}
}
