﻿using UnityEngine;
using System.Collections;

public class ButtonPress : MonoBehaviour 
{
	public float pressOffset = 0.02f;
	public Color32 pressColor = new Color32(200, 200, 200, 255);
	public bool changePos = true;
	public bool changeColor = true;
	public bool locked = false;

	private SpriteRenderer spriteRenderer;
	private Vector3 sourcePos;
	private Color32 sourceColor;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}
	
	void OnMouseDown()
	{
		if (locked)
		{
			return;
		}
			
		sourcePos = transform.position;
		sourceColor = spriteRenderer.color;

		if (changePos)
		{
			Vector3 pressPos = transform.position;
			pressPos.x = pressPos.x + pressOffset;
			pressPos.y = pressPos.y - pressOffset;
			transform.position = pressPos;
		}

		if (changeColor)
		{
			spriteRenderer.color = pressColor;
		}
	}

	void OnMouseUp()
	{
		if (locked)
		{
			return;
		}
	
		if (changePos)
		{
			transform.position = sourcePos;
		}

		if (changeColor)
		{
			spriteRenderer.color = sourceColor;
		}
	}
}
