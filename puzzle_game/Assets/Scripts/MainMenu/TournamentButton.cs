﻿using UnityEngine;
using System.Collections;

public class TournamentButton : MonoBehaviour 
{
	public GameObject mainFrame;
	public GameObject recordsFrame;
	public AudioClip clickSound;
	public bool blocked = false;

	void OnMouseDown()
	{
		if (!blocked)
		{
			SoundManager.instance.PlayMenuClickSound();
		}
	}

	void OnMouseUp()
	{
		if (!blocked)
		{
			mainFrame.SetActive(false);
			recordsFrame.SetActive(true);
		}
	}
}
