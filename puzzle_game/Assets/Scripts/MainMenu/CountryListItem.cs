﻿using UnityEngine;
using System.Collections;

public class CountryListItem : MonoBehaviour 
{	
	public CountryList countryList;
	
	private TextMesh textMesh;

	void Start()
	{
		textMesh = GetComponent<TextMesh>();
	}
	
	public void EchoName()
	{
		SoundManager.instance.PlayMenuClickSound();
		
		countryList.SelectCountry(textMesh.text);
	}
}
