﻿using UnityEngine;

using System;
using System.Collections;

public class NewsBlock : MonoBehaviour 
{
	private SpriteRenderer spriteRenderer;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		//spriteRenderer.sprite = AdManager.instance.newsSprite;

		if (PlayerPrefs.HasKey("newsImageStr"))
		{
			string temp = PlayerPrefs.GetString("newsImageStr");
			
			int width = PlayerPrefs.GetInt("newsImageStr_w");
			int height = PlayerPrefs.GetInt("newsImageStr_h");
			
			Debug.Log(width.ToString());
			Debug.Log(height.ToString());
			
			byte[] byteArray = Convert.FromBase64String(temp);
			//byte[] byteArray = AdManager.instance.testByteArray;
			
			Texture2D tex = new Texture2D(width, height, TextureFormat.ARGB32, false);
			
			tex.LoadImage(byteArray);
			
			Sprite newsSprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height),
			                                  new Vector2(0.5f, 0.5f), 100.0f);
			
			spriteRenderer.sprite = newsSprite;
		}
	}
}
