﻿using UnityEngine;
using System.Collections;

public class SelectCountryField : MonoBehaviour 
{
	public GameObject countryFrame;
	
	private TextMesh textMesh;
	private bool isCountrySet;
	
	void Start()
	{
		textMesh = transform.Find("text").GetComponent<TextMesh>();
		
		if (PlayerPrefs.HasKey("PlayerCountry"))
		{
			isCountrySet = true;
			textMesh.text = PlayerPrefs.GetString("PlayerCountry");
		}
		else
		{
			isCountrySet = false;
		}
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}
	
	void OnMouseUp()
	{
		transform.parent.gameObject.SetActive(false);
		countryFrame.SetActive(true);
	}	
	
	public void SetCoutntry(string countryName)
	{
		isCountrySet = true;
		textMesh.text = countryName;
	}
	
	public string GetCountry()
	{
		if (isCountrySet)
		{
			return textMesh.text;
		}
		else
		{
			return "";
		}
	}
}
