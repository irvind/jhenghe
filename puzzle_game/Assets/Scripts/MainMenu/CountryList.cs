﻿using UnityEngine;

using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CountryList : MonoBehaviour 
{
	public TextAsset countryListFile;
	public GameObject countryNamePrefab;
	public Swiper swiper;
	public GameObject statisticsFrame;
	public SelectCountryField selectCountryField;
	
	public float distance;
	public float swipeSpeed;
	public float ignoreOnStartTime = 0.25f;
	
	private int touchId;
	private bool moved;
	private bool firstFrame;
	private int ignoredTouchId;
	private float time;
	
	private float testY = 0f;

	void Start() 
	{
		touchId = -1;
		ignoredTouchId = -1;
		moved = false;
		firstFrame = true;
		
		
		
		LoadCountryListFile();
	}
	
	void Update() 
	{
		time += Time.deltaTime;
		
		if (firstFrame)
		{
			firstFrame = false;
			return;
		}
	
		if (time > ignoreOnStartTime)
		{
			CheckFinger();
		}
		
		MoveList();
	}
	
	void OnGUI()
	{
		GUILayout.Label(testY.ToString());
	}
	
	void OnEnable()
	{
		Debug.Log("Country list on enable");
		
		transform.localPosition = Vector3.zero;
		time = 0f;
		
		/*
		if (Input.touchCount == 1)
		{
			ignoredTouchId = Input.GetTouch(0).fingerId;
		}
		*/
	}
	
	public void SelectCountry(string countryName)
	{
		selectCountryField.SetCoutntry(countryName);
		
		transform.parent.gameObject.SetActive(false);
		statisticsFrame.SetActive(true);
	}
	
	private void CheckFinger()
	{
		Vector2 screenCoord = Vector2.zero;
		
		if (Input.touchCount == 1)
		{
			Touch touch = Input.GetTouch(0);
		
			/*
			if (touch.fingerId == ignoredTouchId)
			{
				Debug.Log(string.Format("Break country list. finger Id: {0}, ignored Id: {1}", 
				    touch.fingerId, ignoredTouchId));
				
				return;
			}
			*/
					
			if (touchId != touch.fingerId)
			{
				touchId = touch.fingerId;
				moved = false;
			}
			
			screenCoord = touch.position;
		}
		else
		{
			touchId = -1;
			moved = false;
		}
		
		if (screenCoord.magnitude > 0.001)
		{
			Touch touch = Input.GetTouch(0);
		
			if (touch.phase == TouchPhase.Moved)
			{
				moved = true;
			}
			
			if (touch.phase == TouchPhase.Ended && !moved)
			{
				RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(screenCoord),
				  	Vector2.zero);
			
				if ((hit.collider != null) && (hit.transform.parent == transform))
				{
					CountryListItem item = hit.transform.gameObject.GetComponent<CountryListItem>();
					item.EchoName();
				}
			}			
		}
	}
	
	private void MoveList()
	{
		if ((Input.touchCount == 1) && (Input.GetTouch(0).phase == TouchPhase.Moved))
		{
			Touch touch = Input.GetTouch(0);
			
			if (touch.phase == TouchPhase.Moved)
			{
				Vector3 pos = transform.position;
				pos.y += -touch.deltaPosition.y * swipeSpeed;
				transform.position = pos;
				
				testY = touch.deltaPosition.y;
			}
		}
	}
	
	private void LoadCountryListFile()
	{
		StringReader reader = new StringReader(countryListFile.text);
		List<string> countryNames = new List<string>();
		
		string line;
		while ((line = reader.ReadLine()) != null)
		{
			line = line.Trim();
			countryNames.Add(line);
		}
		
		for (int i = 0; i < countryNames.Count; i++)
		{
			GameObject obj = (GameObject)Instantiate(countryNamePrefab);
			obj.GetComponent<TextMesh>().text = countryNames[i];
			obj.GetComponent<CountryListItem>().countryList = this;
			
			obj.transform.parent = transform;
			obj.transform.localPosition = new Vector3(0f, i * distance * -1f, 0f);
		}
	}
}
