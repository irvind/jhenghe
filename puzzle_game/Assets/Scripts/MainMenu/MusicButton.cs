﻿using UnityEngine;
using System.Collections;

public class MusicButton : MonoBehaviour 
{
	public float pressOffset = 0.02f;
	public Color32 pressColor = new Color32(200, 200, 200, 255);
	
	private SpriteRenderer spriteRenderer;
	private Vector3 sourcePos;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		
		if (GameSettings.musicEnabled) 
		{
			spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
		else
		{
			spriteRenderer.color = pressColor;
		}
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();

		sourcePos = transform.position;
		
		Vector3 pressPos = transform.position;
		pressPos.x = pressPos.x + pressOffset;
		pressPos.y = pressPos.y - pressOffset;
		transform.position = pressPos;
		
		if (GameSettings.musicEnabled) 
		{
			spriteRenderer.color = pressColor;
		}
		else
		{
			spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
	}
	
	void OnMouseUp()
	{
		transform.position = sourcePos;
		
		GameSettings.musicEnabled = !GameSettings.musicEnabled;
		PlayerPrefs.SetInt("MusicEnabled", GameSettings.musicEnabled ? 1 : 0);
		
		Debug.Log(PlayerPrefs.GetInt("MusicEnabled").ToString());
	}
}
