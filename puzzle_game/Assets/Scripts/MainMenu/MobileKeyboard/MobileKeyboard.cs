﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public interface IMobileKeyboardListener
{
	void OnKeyClick(string key);
	void OnBackspaceClick();
	void OnKeyboardHide();
}

public class MobileKeyboard : MonoBehaviour 
{
	public float speed;
	public float activeY;
	public float deactiveY;
	
	//private string currentText;
	private bool shift;
	private bool isActive;
	private bool inTransition;
	
	private List<IMobileKeyboardListener> listeners;

	void Start() 
	{
		//currentText = "";
		shift = false;
		isActive = false;
		inTransition = false;
		
		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		deactiveY = cameraBounds.Bottom - 2f;
		
		Vector3 pos = transform.position;
		pos.y = deactiveY;
		transform.position = pos;
		
		listeners = new List<IMobileKeyboardListener>();
	}
	
	void Update()
	{
		if (isActive && Input.touchCount == 1)
		{
			Touch touch = Input.GetTouch(0);
			
			if (touch.phase == TouchPhase.Began)
			{
				RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position),
					Vector2.zero);
					
				if ((hit.collider == null) || (hit.transform.parent != transform))
				{
					Hide();
				}
			}
		}
	}
	
	public void OnCharButtonClick(string charKey)
	{
		if (Regex.IsMatch(charKey, "[A-Z]"))
		{
			if (shift)
			{
				charKey = charKey.ToUpper();
			}
			else
			{
				charKey = charKey.ToLower();
			}
		}
		
		foreach (IMobileKeyboardListener listener in listeners)
		{
			listener.OnKeyClick(charKey);
		}
		
		Debug.Log("Keyboard key: " + charKey);
	}
	
	public void OnShiftButtonClick(bool newShift)
	{
		shift = newShift;
	}
	
	public void OnBackspaceButtonClick()
	{
		/*
		if (currentText.Length != 0)
		{
			currentText = currentText.Substring(0, currentText.Length - 1);
		}
		*/
		
		foreach (IMobileKeyboardListener listener in listeners)
		{
			listener.OnBackspaceClick();
		}
		
		Debug.Log("Keyboard backspace press");
	}
	
	/*
	public string GetCurrentText()
	{
		return currentText;
	}
	*/
	
	public void Show()
	{
		if (!inTransition)
		{
			StartCoroutine(MoveCoroutine(true));
		}
	}
	
	public void Hide()
	{
		if (!inTransition)
		{
			StartCoroutine(MoveCoroutine(false));
			
			foreach (IMobileKeyboardListener listener in listeners)
			{
				listener.OnKeyboardHide();
			}
			
			RemoveListeners();
		}
	}
	
	public void AddListener(IMobileKeyboardListener listener)
	{
		listeners.Add(listener);
	}
	
	public void RemoveListeners()
	{
		listeners.Clear();
	}
	
	private IEnumerator MoveCoroutine(bool destState)
	{
		inTransition = true;
		
		float destY;
		if (destState)
		{
			destY = activeY;
		}
		else
		{
			destY = deactiveY;
		}
		
		while (true)
		{
			Vector3 pos = transform.position;
			pos.y = Mathf.Lerp(pos.y, destY, speed * Time.deltaTime);
			transform.position = pos;
			
			if (Mathf.Abs(pos.y - destY) > 0.05f)
			{
				yield return null;
			}
			else
			{
				break;
			}
		}
		
		if (destState)
		{
			isActive = true;
		}
		else
		{
			isActive = false;
		}
		
		inTransition = false;
	}
}
