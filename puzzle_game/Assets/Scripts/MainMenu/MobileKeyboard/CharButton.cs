﻿using UnityEngine;
using System.Collections;

public class CharButton : MonoBehaviour 
{
	public TextMesh textMesh;
	
	private string charKey;
	private MobileKeyboard keyboard;
	
	void Start()
	{
		keyboard = GameObject.FindGameObjectWithTag("Keyboard").GetComponent<MobileKeyboard>();
		charKey = textMesh.text;
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
		
		keyboard.OnCharButtonClick(charKey);
	}
}
