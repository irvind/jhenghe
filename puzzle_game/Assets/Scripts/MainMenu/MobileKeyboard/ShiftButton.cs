﻿using UnityEngine;
using System.Collections;

public class ShiftButton : MonoBehaviour 
{
	public Color32 pressColor = new Color32(200, 200, 200, 255);
	
	private SpriteRenderer spriteRenderer;
	private MobileKeyboard keyboard;
	
	private bool shiftOn;
	
	void Start () 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		keyboard = GameObject.FindGameObjectWithTag("Keyboard").GetComponent<MobileKeyboard>();
		
		shiftOn = false;
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
		
		shiftOn = !shiftOn;
		if (shiftOn)
		{
			spriteRenderer.color = pressColor;
		}
		else
		{
			spriteRenderer.color = Color.white;
		}
		
		keyboard.OnShiftButtonClick(shiftOn);
	}
}
