﻿using UnityEngine;
using System.Collections;

public class BackSpaceButton : MonoBehaviour 
{	
	private MobileKeyboard keyboard;
	
	void Start () 
	{
		keyboard = GameObject.FindGameObjectWithTag("Keyboard").GetComponent<MobileKeyboard>();
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
		
		keyboard.OnBackspaceButtonClick();
	}
}
