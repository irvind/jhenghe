﻿using UnityEngine;
using System.Collections;

public class HideKeyboardButton : MonoBehaviour 
{
	private MobileKeyboard keyboard;
	
	void Start() 
	{
		keyboard = GameObject.FindGameObjectWithTag("Keyboard").GetComponent<MobileKeyboard>();
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();	
	}
	
	void OnMouseUp()
	{
		keyboard.Hide();
	}
}
