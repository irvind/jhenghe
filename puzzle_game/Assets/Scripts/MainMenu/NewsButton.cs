﻿using UnityEngine;
using System.Collections;

public class NewsButton : MonoBehaviour 
{
	public AudioClip clickSound;
	public GameObject mainFrame;
	public GameObject newsFrame;

	void OnMouseDown()
	{
		//AudioSource.PlayClipAtPoint(clickSound, transform.position);
		SoundManager.instance.PlayMenuClickSound();
	}
	
	void OnMouseUp()
	{
		mainFrame.SetActive(false);
		newsFrame.SetActive(true);
	}
}
