﻿using UnityEngine;
using System.Collections;

public class SubmitRatingButton : MonoBehaviour 
{
	public NameField nameField;
	public SelectCountryField selectCountryField;

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}
	
	void OnMouseUp()
	{
		string nameText = nameField.GetName();
		string countryText = selectCountryField.GetCountry();
		
		if (nameText != "")
		{
			PlayerPrefs.SetString("PlayerName", nameText);
			
			if (countryText != "")
			{
				PlayerPrefs.SetString ("PlayerCountry", countryText);
			}
			
			if (countryText == "")
			{
				countryText = "none";
			}
			
			string appId = PlayerPrefs.GetString("AppInstanceId");
			
			StartCoroutine(UpdateRatingInfo(nameText, countryText, appId));
			StartCoroutine(OpenRatingPage(nameText));
		}
		else
		{
			Application.OpenURL("http://game.troi.pro/zhenghe/");
		}
	}
	
	private IEnumerator UpdateRatingInfo(string name, string country, string appId)
	{
		WWWForm form = new WWWForm();
		form.AddField("player_name", name);
		form.AddField("country", country);
		form.AddField("app_id", appId);
		
		WWW www = new WWW("http://game.troi.pro/rating/update_rating_info.php", form);
		yield return www;
	}
	
	private IEnumerator OpenRatingPage(string name = "")
	{
		yield return new WaitForSeconds(0.25f);
		
		if (name != "")
		{
			Application.OpenURL("http://game.troi.pro/zhenghe?name=" + name);
		}
		else
		{
			Application.OpenURL("http://game.troi.pro/zhenghe");
		}
	}
}
