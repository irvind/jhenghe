﻿using UnityEngine;
using System.Collections;

public class StatisticsButton : MonoBehaviour 
{
	public GameObject mainFrame;
	public GameObject regitrationFrame;
	
	public AudioClip clickSound;
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}
	
	void OnMouseUp()
	{
		mainFrame.SetActive(false);
		regitrationFrame.SetActive(true);
	}
}
