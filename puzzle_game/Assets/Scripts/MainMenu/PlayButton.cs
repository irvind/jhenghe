﻿using UnityEngine;
using System.Collections;

public class PlayButton : MonoBehaviour 
{
	private ScreenChanger screenChanger;

	void Start()
	{
		screenChanger = GameObject.FindGameObjectWithTag("ScreenChanger").GetComponent<ScreenChanger>();
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}

	void OnMouseUp()
	{
		//Application.LoadLevel(3);
		screenChanger.GoToScreen(3);
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.Quit();
		}
	}
}
