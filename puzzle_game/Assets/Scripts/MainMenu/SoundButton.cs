﻿using UnityEngine;
using System.Collections;

public class SoundButton : MonoBehaviour 
{
	public float pressOffset = 0.02f;
	public Color32 pressColor = new Color32(200, 200, 200, 255);

	private SpriteRenderer spriteRenderer;
	private Vector3 sourcePos;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();

		if (GameSettings.soundEnabled) 
		{
			spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
		else
		{
			spriteRenderer.color = pressColor;
		}
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();

		sourcePos = transform.position;

		Vector3 pressPos = transform.position;
		pressPos.x = pressPos.x + pressOffset;
		pressPos.y = pressPos.y - pressOffset;
		transform.position = pressPos;

		if (GameSettings.soundEnabled) 
		{
			spriteRenderer.color = pressColor;
		}
		else
		{
			spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
	}

	void OnMouseUp()
	{
		transform.position = sourcePos;

		GameSettings.soundEnabled = !GameSettings.soundEnabled;
		PlayerPrefs.SetInt("SoundEnabled", GameSettings.soundEnabled ? 1 : 0);
	}
}
