﻿using UnityEngine;
using System.Collections;

public class Swiper : MonoBehaviour 
{
	private float prevY = -1f;
	private float resetY = -1f;
	
	private float currentY = -1f;
	
	void Update() 
	{
		SwipeTouch();
	}
	
	/*
	void OnGUI()
	{
		GUILayout.Label(prevY.ToString());
		GUILayout.Label(GetSwipeVal().ToString());
		Debug.Log(GetSwipeVal().ToString());
	}
	*/
	
	public float GetSwipeVal()
	{
		if ((prevY != resetY) || (currentY != resetY))
		{
			return currentY - prevY;
		}
		else
		{
			//Debug.Log("Is null");
			return 0f;
		}
	}
	
	private void SwipeTouch()
	{
		if (Input.touches.Length == 1)
		{
			Touch t = Input.GetTouch(0);
			
			prevY = currentY;
			currentY = t.position.y / Screen.height;
		}
		else
		{
			prevY = resetY;
			currentY = resetY;
		}
	}
}
