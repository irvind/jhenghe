﻿using UnityEngine;
using System.Collections;

public class TroiButton : MonoBehaviour 
{
	public AudioClip clickSound;

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}

	void OnMouseUp()
	{
		Application.OpenURL("http://178.19.248.3/troi/");
	}
}
