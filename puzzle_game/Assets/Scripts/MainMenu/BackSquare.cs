﻿using UnityEngine;
using System.Collections;

public class BackSquare : MonoBehaviour 
{
	public GameObject currentFrame;
	public GameObject previousFrame;
	
	private SpriteRenderer spriteRenderer;
	private MobileKeyboard keyboard;
	
	void Start()
	{
		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		
		Vector3 pos = transform.position;
		pos.x = 3.76f;
		pos.y = cameraBounds.Top + 0.02f - 0.42f - 0.25f;
		transform.position = pos;
		
		spriteRenderer = GetComponent<SpriteRenderer>();
		keyboard = GameObject.FindGameObjectWithTag("Keyboard").GetComponent<MobileKeyboard>();
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
		
		spriteRenderer.color = new Color32(255, 152, 0, 255);
	}
	
	void OnMouseUp()
	{
		currentFrame.SetActive(false);
		previousFrame.SetActive(true);
		
		spriteRenderer.color = new Color32(32, 32, 32, 255);
		keyboard.Hide();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			currentFrame.SetActive(false);
			previousFrame.SetActive(true);
		}
	}
}
