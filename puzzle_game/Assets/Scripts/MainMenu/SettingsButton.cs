﻿using UnityEngine;
using System.Collections;

public class SettingsButton : MonoBehaviour 
{
	public GameObject mainFrame;
	public GameObject settingsFrame;
	public AudioClip clickSound;

	void OnMouseDown()
	{
		//AudioSource.PlayClipAtPoint(clickSound, transform.position);
		SoundManager.instance.PlayMenuClickSound();
	}

	void OnMouseUp()
	{
		mainFrame.SetActive(false);
		settingsFrame.SetActive(true);
	}
}
