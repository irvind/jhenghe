﻿using UnityEngine;
using System.Collections;

public class NameField : MonoBehaviour, IMobileKeyboardListener
{
	public TextMesh textMesh;
	public Color32 pressColor = new Color32(200, 200, 200, 255);
	
	private MobileKeyboard keyboard;
	private SpriteRenderer spriteRenderer;
	private bool useKeyboard;
	private bool defaultText;
	
	void Start()
	{
		keyboard = GameObject.FindGameObjectWithTag("Keyboard").GetComponent<MobileKeyboard>();
		spriteRenderer = GetComponent<SpriteRenderer>();
		
		useKeyboard = false;
		
		if (PlayerPrefs.HasKey("PlayerName"))
		{
			textMesh.text = PlayerPrefs.GetString("PlayerName");
			defaultText = false;
		}
		else
		{
			defaultText = true;		
		}
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
		
		useKeyboard = !useKeyboard;
		if (useKeyboard)
		{
			spriteRenderer.color = pressColor;
			keyboard.Show();
			keyboard.AddListener(this);
		}
	}
	
	public string GetName()
	{
		if (defaultText)
		{
			return "";
		}
		
		return textMesh.text;
	}
	
	public void OnKeyClick(string key)
	{
		if (defaultText)
		{
			textMesh.text = key;
			defaultText = false;
		}
		else
		{
			textMesh.text = textMesh.text + key;
		}
	}
	
	public void OnBackspaceClick()
	{
		if (!defaultText)
		{
			string currentText = textMesh.text;
			
			if (currentText.Length > 0)
			{
				currentText = currentText.Substring(0, currentText.Length - 1);
			}
			
			if (currentText.Length == 0)
			{
				currentText = "Enter your name...";
				defaultText = true;
			}
			
			textMesh.text = currentText;
		}
	}
	
	public void OnKeyboardHide()
	{
		useKeyboard = false;
		spriteRenderer.color = Color.white;
		
		//keyboard.RemoveListeners();
	}
}
