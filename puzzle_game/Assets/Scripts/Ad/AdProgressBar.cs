﻿using UnityEngine;
using System.Collections;

public class AdProgressBar : MonoBehaviour 
{
	public SpriteRenderer adImageRenderer;
	
	private ScreenChanger screenChanger;
	
	public float timeAmount;
	private float time;
	
	private bool trig = false;

	void Start() 
	{
		screenChanger = GameObject.FindGameObjectWithTag("ScreenChanger").GetComponent<ScreenChanger>();
		
		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();

		Vector3 pos = transform.position;
		pos.y = cameraBounds.Bottom + 0.4f;
		transform.position = pos;

		time = 0;
		
		int screenId;
		adImageRenderer.sprite = AdManager.instance.TakeAdScreen(out screenId);
		StatisticsManager.instance.AddAdScreenView(screenId);
	}

	void Update() 
	{
		time = time + Time.deltaTime;

		if ((time > timeAmount) && !trig)
		{
			//screenChanger.GoToScreen(2);
			screenChanger.ChangeAsyncLevel();
			
			trig = true;
		}
		else
		{
			transform.localScale = new Vector3(time / timeAmount, 1, 1);
		}
	}
}
