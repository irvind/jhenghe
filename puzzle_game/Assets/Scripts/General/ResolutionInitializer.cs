﻿using UnityEngine;
using System.Collections;

public class ResolutionInitializer : MonoBehaviour 
{
	public int width;
	public int height;

	public int testW;
	public int testH;
	public float testAspect;

	public bool showResolutionGUI = false;
	public bool setResolution = true;
	public bool setOrientation = true;
	public ScreenOrientation screenOrientation;

	//private float waitTime = 0.05f;

	void Awake() 
	{
		if (setResolution)
		{
			Screen.SetResolution(width, height, false);
		}

		if (setOrientation)
		{
			Screen.orientation = screenOrientation;
		}

		DontDestroyOnLoad(gameObject);
	}

	void Start()
	{
		Camera camera = GameObject.FindGameObjectWithTag("MainCamera").camera;
		
		testW = Screen.width;
		testH = Screen.height;
		
		float aspectRatio = (float)Screen.width / (float)Screen.height;
		testAspect = aspectRatio;
		
		camera.orthographicSize = 11.36f / (2 * aspectRatio);
		
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
	}

	void OnGUI()
	{
		if (showResolutionGUI)
		{
			string str = string.Format("{0} {1}", Screen.width, Screen.height);
			//string str = string.Format("WxH: {0} {1}", testW, testH);
			GUILayout.Label(str);

			//GUILayout.Label(testAspect.ToString());
			GUILayout.Label(camera.orthographicSize.ToString());
		}
	}
}
