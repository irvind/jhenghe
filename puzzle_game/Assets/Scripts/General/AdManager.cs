﻿using UnityEngine;
using System;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class AdManager : MonoBehaviour 
{
	public static AdManager instance;

	[HideInInspector]
	public Texture2D newsTexture;
	
	[HideInInspector]
	public Sprite newsSprite;
	public bool newsLoaded = false;
	public bool newsLoadingNow = false;
	
	public bool adFramesLoaded = false;
	public bool adFramesLoadingNow = false;
	
	public Sprite[] adFramesSprites;
	
	public bool checkOnStart;
	public float checkPeriod;

	private string newsImageUrl = "";
	private string newsInfoFileUrl = "http://game.troi.pro/control_panel/news.txt";
	
	private int currentAdFrame;
	private float time;

	void Start() 
	{
		instance = this;

		if (!PlayerPrefs.HasKey("NewsPictureVersion"))
		{
			PlayerPrefs.SetInt("NewsPictureVersion", 0);
		}
		
		currentAdFrame = 0;
		time = 0f;
		
		if (checkOnStart)
		{
			CheckUpdates();
		}
	}

	void Update()
	{
		time += Time.deltaTime;
		
		if (time > checkPeriod)
		{
			time = 0f;
			
			CheckUpdates();
		}
	}
	
	private void CheckUpdates()
	{
		if (!newsLoaded && !newsLoadingNow)
		{
			newsLoadingNow = true;
			StartCoroutine("LoadNewsInfo");
		}
	}
	
	public Sprite TakeAdScreen(out int screenId)
	{
		Sprite result = adFramesSprites[currentAdFrame];
		
		currentAdFrame++;
		if (currentAdFrame >= adFramesSprites.Length)
		{
			currentAdFrame = 0;
		}
		
		screenId = currentAdFrame;
		return result;
	}

	IEnumerator LoadNewsInfo()
	{
		WWW www = new WWW(newsInfoFileUrl);

		yield return www;
		
		if (www.error != null)
		{
			Debug.Log("Cannot load news info");
			newsLoadingNow = false;
			yield break;
		}

		string[] split = www.text.Split(' ');
		int newsVersion = int.Parse(split[0]);

		if (PlayerPrefs.GetInt("NewsPictureVersion") < newsVersion)
		{
			Debug.Log("News available");
			
			newsImageUrl = "http://game.troi.pro/control_panel/" + split[1];
			StartCoroutine("LoadNewsImage");
		}
		else
		{
			newsLoaded = true;
			newsLoadingNow = false;
		}
	}
	
	IEnumerator LoadNewsImage()
	{	
		WWW www = new WWW(newsImageUrl);

		yield return www;

		if (www.error != null)
		{
			Debug.Log("Cannot load news image");
			newsLoadingNow = false;
			yield break;
		}

		newsTexture = www.texture;
		newsSprite = Sprite.Create(newsTexture, new Rect(0, 0, newsTexture.width, newsTexture.height),
		    new Vector2(0.5f, 0.5f), 100.0f);

		byte[] byteArray;
		byteArray = newsTexture.EncodeToPNG();
		string temp = Convert.ToBase64String(byteArray);

		PlayerPrefs.SetString("newsImageStr", temp); 
		PlayerPrefs.SetInt("newsImageStr_w", newsTexture.width);
		PlayerPrefs.SetInt("newsImageStr_h", newsTexture.height);

		PlayerPrefs.SetInt("NewsPictureVersion", PlayerPrefs.GetInt("NewsPictureVersion") + 1);
		
		newsLoaded = true;
		newsLoadingNow = false;
	}

	IEnumerator LoadAdFramesInfo()
	{
		yield return true;
	}
}
