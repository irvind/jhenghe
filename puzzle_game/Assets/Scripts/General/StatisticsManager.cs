﻿using UnityEngine;

using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class StatData
{
	public int runs;
	public int puzzleSolved;
	public Dictionary<int, int> adScreenViews;
	
	public StatData()
	{
		adScreenViews = new Dictionary<int, int>();
		
		runs = 0;
		puzzleSolved = 0;
	}

	public bool IsUnsended()
	{
		if ((runs != 0) || (puzzleSolved != 0) || (adScreenViews.Count != 0))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public void Clear()
	{
		runs = 0;
		puzzleSolved = 0;

		adScreenViews.Clear();
	}
}

public class StatisticsManager : MonoBehaviour 
{
	public static StatisticsManager instance;

	public float checkPeriod;

	private string statUpdateUrl = "http://game.troi.pro/control_panel/update_stat.php";
	private StatData statData;
	private bool sendingData;
	private float time;
	
	void Start() 
	{
		instance = this;

		time = 0f;
		sendingData = false;

		if (!LoadStatInfo())
		{
			//Debug.Log("No saved stat");
			statData = new StatData();
		}
		else
		{
			//Debug.Log("Have saved stat");
		}
		
		statData.runs++;
		
		if (statData.IsUnsended())
		{
			//Debug.Log("Stat data is unsended");
			StartCoroutine(SendStatInfo(true));
		}
	}
	
	void Update() 
	{
		time += Time.deltaTime;

		if (time > checkPeriod)
		{
			time = 0f;

			if (!sendingData && statData.IsUnsended())
			{
				StartCoroutine(SendStatInfo(true));
				//Debug.Log("Check period. Send");
			}
			else
			{
				//Debug.Log("Check period. Not send");
			}
		}
	}
	
	public void AddRun()
	{
		//Debug.Log("Add run");
		statData.runs++;
		
		if (!sendingData)
		{
			//Debug.Log("Add run send");
			StartCoroutine(SendStatInfo(false));
		}
	}
	
	public void AddPuzzleSolve()
	{
		//Debug.Log("Add puzzle solve");
		statData.puzzleSolved++;

		if (!sendingData)
		{
			//Debug.Log("Add puzzle solve send");
			StartCoroutine(SendStatInfo(false));
		}
	}
	
	public void AddAdScreenView(int screenId)
	{
		//Debug.Log("Add ad screen view");
		if (statData.adScreenViews.ContainsKey(screenId))
		{
			statData.adScreenViews[screenId]++;
		}
		else
		{
			statData.adScreenViews.Add(screenId, 1);
		}

		if (!sendingData)
		{
			//Debug.Log("Add ad screen view send");
			StartCoroutine(SendStatInfo(false));
		}
	}
	
	private void SaveStatInfo()
	{
		//Debug.Log("Save stat info");
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/stat.dat");
		
		binaryFormatter.Serialize(file, statData);
		file.Close();	
	}
	
	private bool LoadStatInfo()
	{
		if (File.Exists(Application.persistentDataPath + "/stat.dat"))
		{
			//Debug.Log("Load stat info. Exists");
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/stat.dat", FileMode.Open);
			
			statData = (StatData)binaryFormatter.Deserialize(file);
			
			file.Close();
			
			return true;
		}
		else
		{
			//Debug.Log("Load stat info. Not exists");
			return false;
		}
	}
	
	private void DeleteStatInfo()
	{
		//Debug.Log("DeleteStatInfo");
		
		string path = Application.persistentDataPath + "/stat.dat";
		if (File.Exists(path))
		{
			File.Delete(path);
		}
	}
	
	private IEnumerator SendStatInfo(bool deleteStatFile = false)
	{
		if (deleteStatFile)
		{
			//Debug.Log("Start sending data. Delete stat file.");
		}
		else
		{
			//Debug.Log("Start sending data. Don't delete stat file.");
		}
		
		sendingData = true;

		WWWForm form = new WWWForm();

		form.AddField("app_id", PlayerPrefs.GetString("AppInstanceId"));
		form.AddField("runs", statData.runs);
		form.AddField("puzzle_solved", statData.puzzleSolved);

		StringBuilder strBuilder = new StringBuilder();
		foreach (KeyValuePair<int, int> pair in statData.adScreenViews)
		{
			strBuilder.AppendFormat("{0} {1} ", pair.Key, pair.Value);
		}

		form.AddField("ad_screen_views", strBuilder.ToString());

		WWW www = new WWW(statUpdateUrl, form);
		yield return www;

		if (www.error == null)
		{
			//Debug.Log("Send successful");
			
			statData.Clear();
			
			if (deleteStatFile)
			{
				DeleteStatInfo();
			}
		}
		else
		{
			//Debug.Log("Send is not successful");
			SaveStatInfo();
		}

		sendingData = false;
		//Debug.Log("End sending data");
	}
}
