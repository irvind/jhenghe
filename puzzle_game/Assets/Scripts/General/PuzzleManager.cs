﻿using UnityEngine;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class PuzzleManager : MonoBehaviour 
{
	public static PuzzleManager instance;
	
	public Puzzle[] puzzles;
	//public Puzzle[] packPuzzles;
	
	public TextAsset puzzleFile;

	public bool serializePuzzles;
	public TextAsset storePuzzleFile;
	
	public bool setPackIndex;
	public int packIndex;
	
	public bool setCurrentLevel;
	public int curLevel;
	
	[HideInInspector]
	public bool downloadFinished;
	
	[HideInInspector]
	public bool downloadStatus;

	[HideInInspector]
	public int loadedPuzzleMin = 0;
	
	[HideInInspector]
	public int loadedPuzzleIndexBorder = 0;

	void Start() 
	{
		instance = this;
		puzzles = null;
		
		if (setCurrentLevel)
		{
			PlayerPrefs.SetInt("CurrentLevel", curLevel);
		}
		
		if (setPackIndex)
		{
			PlayerPrefs.SetInt("PuzzlePackIndex", packIndex);
		}
		
		Debug.Log("Current level: " + PlayerPrefs.GetInt("CurrentLevel").ToString());
		Debug.Log("Puzzle pack index: " + PlayerPrefs.GetInt("PuzzlePackIndex").ToString());

		LoadCurrentPack();
		
		if (serializePuzzles)
		{
			SavePuzzlePack();
		}
		
		//Debug.Log(HavePuzzle().ToString());
		//DownloadPuzzlePack();	
	}
	
	public bool HavePuzzle()
	{
		if (puzzles != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public Puzzle GetPuzzle()
	{
		if (puzzles != null)
		{
			int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
			return puzzles[currentLevel - loadedPuzzleMin];
		}
		else
		{
			return null;
		}
	}
	
	public void NextPuzzle()
	{
		if (puzzles == null)
		{
			return;
		}

		int currentLevel = PlayerPrefs.GetInt("CurrentLevel");
		currentLevel++;

		PlayerPrefs.SetInt("CurrentLevel", currentLevel);

		if (currentLevel >= loadedPuzzleIndexBorder)
		{
			int puzzlePackIndex = PlayerPrefs.GetInt("PuzzlePackIndex");
			int border = 300 + (puzzlePackIndex + 1) * 300;

			if (currentLevel >= border)
			{
				puzzles = null;
			}
			else
			{
				int index = currentLevel / 300 - 1;
				LoadPuzzlePack(index);
			}
		}
	}
	
	public void DownloadPuzzlePack()
	{
		Debug.Log("Start download");
	
		int puzzlePackIndex = PlayerPrefs.GetInt("PuzzlePackIndex");
		int nextPuzzlePackIndex = puzzlePackIndex + 1;
		
		string url = "http://game.troi.pro/puzzle_packs/pack" + nextPuzzlePackIndex.ToString()
			+ ".dat";
		
		downloadFinished = false;
		downloadStatus = false;
		
		StartCoroutine(PuzzlePackCoroutine(url, nextPuzzlePackIndex));
	}
	
	public void ResetPuzzles()
	{
		PlayerPrefs.SetInt("CurrentLevel", 0);
		LoadBasePuzzles();
	}
	
	public void LoadCurrentPack()
	{
		int currentPuzzle = PlayerPrefs.GetInt("CurrentLevel");
		if (currentPuzzle < 300)
		{
			LoadBasePuzzles();
		}
		else
		{
			//Debug.Log("test");
			
			int puzzlePackIndex = PlayerPrefs.GetInt("PuzzlePackIndex");
			
			int start = 300 + puzzlePackIndex * 300;
			int end = start + 300;
			
			if (currentPuzzle < end)
			{		
				LoadPuzzlePack(puzzlePackIndex);
			}
		}
		
		if (puzzles != null)
		{
			Debug.Log("LoadCurrentPack puzzle length: " + puzzles.Length.ToString());
		}
	}
	
	private IEnumerator PuzzlePackCoroutine(string url, int nextPackIndex)
	{
		WWW www = new WWW(url);
		yield return www;
		
		downloadFinished = true;
		
		if (www.error == null)
		{
			string filepath = Application.persistentDataPath + "/puzzle_pack" 
				+ nextPackIndex.ToString().PadLeft(3, '0') + ".dat";
			
			File.WriteAllBytes(filepath, www.bytes);
			
			downloadStatus = true;
			
			PlayerPrefs.SetInt("PuzzlePackIndex", nextPackIndex);
		}
		else
		{
			downloadStatus = false;
		}
		
		Debug.Log("Download status: " + downloadStatus.ToString());
		Debug.Log(PlayerPrefs.GetInt("PuzzlePackIndex"));
	}
	
	private void LoadBasePuzzles()
	{
		StringReader reader = new StringReader(puzzleFile.text);
		
		puzzles = LoadPuzzlesFromReader(reader);
		loadedPuzzleMin = 0;
		loadedPuzzleIndexBorder = 300;
	}
	
	private void LoadPuzzlePack(int puzzlePackIndex)
	{
		if (puzzlePackIndex != -1)
		{
			string packFilename = "puzzle_pack" + puzzlePackIndex.ToString().PadLeft(3, '0') + ".dat";
			
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/" + packFilename, FileMode.Open);
			
			puzzles = (Puzzle[])binaryFormatter.Deserialize(file);
			loadedPuzzleMin = 300 + puzzlePackIndex * 300;
			loadedPuzzleIndexBorder = 300 + (puzzlePackIndex + 1) * 300;
			
			file.Close();
		}
	}
	
	private void SavePuzzlePack()
	{
		StringReader reader = new StringReader(storePuzzleFile.text);
		Puzzle[] storePuzzles = LoadPuzzlesFromReader(reader);
		
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/store_puzzles.dat");
		
		binaryFormatter.Serialize(file, storePuzzles);
		file.Close();	
	}
	
	private Puzzle[] LoadPuzzlesFromReader(StringReader reader)
	{
		char[] separators = new char[] {' '};
		
		List<Puzzle> testPuzzleList = new List<Puzzle>();
		
		string line;
		while ((line = reader.ReadLine()) != null)
		{
			if (line.Length < 3) 
			{
				continue;
			}
			
			Puzzle testPuzzle = new Puzzle();
			
			string[] splitArr = line.Split(separators);
			int splitIndex = 0;
			
			if (splitArr[0] == "simple")
			{
				//Debug.Log("Simple puzzle detected");
				testPuzzle.isSimple = true;
				testPuzzle.openedFigures = new int[] {2};
				
				testPuzzle.AssignPair(5, FigureType.TYPE_0);
				testPuzzle.AssignPair(0, FigureType.TYPE_1);
				testPuzzle.AssignPair(1, FigureType.TYPE_2);
				
				testPuzzleList.Add(testPuzzle);
				
				continue;
			}
			
			List<string> lNumbers = new List<string>();
			List<string> lHorOperations = new List<string>();
			List<string> lVertOperations = new List<string>();
			List<int> lOpenedFigures = new List<int>();
			
			for (int i = 0; i < 9; i++) 
			{
				lNumbers.Add(splitArr[splitIndex]);
				splitIndex++;
			}
			
			for (int i = 0; i < 6; i++)
			{
				if (i < 3)
				{
					lHorOperations.Add(splitArr[splitIndex]);
				}
				else
				{
					lVertOperations.Add(splitArr[splitIndex]);
				}
				
				splitIndex++;
			}
			
			int figurePairsLen = int.Parse(splitArr[splitIndex++]);
			
			for (int i = 0; i < figurePairsLen; i++)
			{
				int number = int.Parse(splitArr[splitIndex]);
				splitIndex += 2;
				int iFigType = int.Parse(splitArr[splitIndex]);
				splitIndex++;
				
				FigureType figType = (FigureType)iFigType;
				
				testPuzzle.AssignPair(number, figType);
			}
			
			int openedFiguresLen = int.Parse(splitArr[splitIndex++]);
			
			for (int i = 0; i < openedFiguresLen; i++)
			{
				lOpenedFigures.Add(int.Parse(splitArr[splitIndex++]));
			}
			
			testPuzzle.numbers = lNumbers.ToArray();
			testPuzzle.horOperations = lHorOperations.ToArray();
			testPuzzle.vertOperations = lVertOperations.ToArray();
			testPuzzle.openedFigures = lOpenedFigures.ToArray();
			
			testPuzzleList.Add(testPuzzle);
		}
		
		return testPuzzleList.ToArray();
	}
}
