﻿using UnityEngine;

using System;
using System.Collections;
using System.Collections.Generic;

[Serializable]
public struct FigureNumberPair
{
	public int number;
	public FigureType figure;
}

[Serializable]
public class Puzzle 
{
	public string[] numbers;
	public string[] horOperations;
	public string[] vertOperations;
	public int[] openedFigures;

	public bool solved;
	public int timeRemains;
	
	public bool isSimple;
	
	private FigureNumberPair[] figureNumberPairs;

	public Puzzle()
	{
		numbers = new string[9];
		horOperations = new string[3];
		vertOperations = new string[3];

		figureNumberPairs = new FigureNumberPair[10];
		for (int i = 0; i < 10; i++) 
		{
			figureNumberPairs[i].number = i;
			figureNumberPairs[i].figure = FigureType.NONE;
		}

		solved = false;
		timeRemains = 3601;
		
		isSimple = false;
	}

	public void AssignPair(int number, FigureType figure)
	{
		if (number < 0 && number > 9) 
		{
			return;
		}

		bool found = false;
		for (int i = 0; i < 10; i++) 
		{
			if (figureNumberPairs[i].figure == figure)
			{
				found = true;
				break;
			}
		}

		if (found)
		{
			return;
		}

		figureNumberPairs[number].figure = figure;
	}

	public FigureType GetNumberType(int number)
	{
		if ((number < 0) || (number > 9))
		{
			return FigureType.NONE;
		}

		return figureNumberPairs[number].figure;
	}

	public int GetTypeNumber(FigureType type)
	{
		int num = -1;

		for (int i = 0; i < figureNumberPairs.Length; i++)
		{
			if (figureNumberPairs[i].figure == type) 
			{
				num = figureNumberPairs[i].number;
				break;
			}
		}

		return num;
	}

	public bool HaveFigureType(FigureType figType)
	{
		for (int i = 0; i < 10; i++) 
		{
			if (figureNumberPairs[i].figure == figType)
			{
				return true;
			}
		}

		return false;
	}

	public int[] GetOpenedNumbers()
	{
		List<int> openedNumbers = new List<int>();

		for (int i = 0; i < openedFigures.Length; i++)
		{
			openedNumbers.Add(GetTypeNumber((FigureType)openedFigures[i]));
		}

		return openedNumbers.ToArray();
	}
}
