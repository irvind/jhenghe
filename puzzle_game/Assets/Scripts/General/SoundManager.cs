﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour 
{
	public AudioClip menuClickSound;
	public float menuClickSoundVolume;

	public AudioClip symbolClickSound;
	public float symbolClickSoundVolume;

	public AudioClip numberClickSound;
	public float numberClickSoundVolume;

	public static SoundManager instance;

	void Start()
	{
		instance = this;
	}

	public void PlayMenuClickSound()
	{
		if (GameSettings.soundEnabled)
		{
			AudioSource.PlayClipAtPoint(menuClickSound, transform.position, menuClickSoundVolume);
		}
	}

	public void PlaySymbolClickSound()
	{
		if (GameSettings.soundEnabled)
		{
			AudioSource.PlayClipAtPoint(symbolClickSound, transform.position, symbolClickSoundVolume);
		}
	}

	public void PlayNumberClickSound()
	{
		if (GameSettings.soundEnabled)
		{
			AudioSource.PlayClipAtPoint(numberClickSound, transform.position, numberClickSoundVolume);
		}
	}
}
