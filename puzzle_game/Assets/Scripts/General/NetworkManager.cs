﻿using UnityEngine;

using System;
using System.Collections;
using System.Net;

public class NetworkManager : MonoBehaviour 
{
	public static NetworkManager instance;

	public float checkPeriod = 60f;
	public bool hasConnection;

	private float time;

	void Start() 
	{
		instance = this;

		hasConnection = false;
		time = 0f;

		//StartCoroutine("CheckIfConnnected");
	}

	void Update() 
	{
		time += Time.deltaTime;

		if (time > checkPeriod)
		{
			time = 0;

			//StartCoroutine("CheckIfConnnected");
		}
	}

	private IEnumerator CheckIfConnnected()
	{
		/*
		HttpWebRequest req = (HttpWebRequest)WebRequest.Create("http://www.google.com");
		hasConnection = true;

		try 
		{
			HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
		}
		catch
		{
			hasConnection = false;
		}
		*/

		yield return true;
	}



}
