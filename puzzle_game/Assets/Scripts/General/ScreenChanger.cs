﻿using UnityEngine;
using System.Collections;

public enum ScreenChangerState
{
	IN, OUT, OFF
}

public class ScreenChanger : MonoBehaviour 
{
	public float fadeSpeed;
	public ScreenChangerState state;
	public bool dontFadeOnStart = false;
	public bool dontFadeOnEnd = false;
	public bool asyncLevelLoad = false;
	public int asyncLevel;
	public bool showTime = false;
	
	private SpriteRenderer spriteRenderer;
	
	private int destScreen;
	private float time;
	
	private bool inEndTransition;
	private AsyncOperation asyncOp;
	
	private float totalTime;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		
		if (!dontFadeOnStart)
		{
			state = ScreenChangerState.IN;
			time = 0;
		
			spriteRenderer.color = new Color(1f, 1f, 1f, 1f);
		}
		else
		{
			state = ScreenChangerState.OFF;
		}
		
		inEndTransition = false;
		
		if (asyncLevelLoad)
		{
			StartCoroutine("LoadLevelInBackground");
		}
		
		totalTime = 0f;
	}
	
	void Update() 
	{
		totalTime += Time.deltaTime;
		
		//Debug.Log(totalTime.ToString());
	
		if (state != ScreenChangerState.OFF)
		{
			time += Time.deltaTime;
			
			if (time > fadeSpeed)
			{
				if (state == ScreenChangerState.IN)
				{
					spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
					state = ScreenChangerState.OFF;
					collider2D.enabled = false;
					time = 0f;
				}
				else
				{
					if (!asyncLevelLoad)
					{
						Application.LoadLevel(destScreen);
					}
					else
					{
						ChangeAsyncLevel();
					}
				}
			}
			else
			{
				Color color = new Color(1f, 1f, 1f);
				
				if (state == ScreenChangerState.IN)
				{
					color.a = 1f - time / fadeSpeed;
				}
				else
				{
					color.a = time / fadeSpeed;
				}
				
				spriteRenderer.color = color;
			}
		}
	}
	
	void OnGUI()
	{
		if (showTime)
		{
			GUILayout.Label(totalTime.ToString());
		}
	}
	
	public void GoToScreen(int screenId)
	{
		if (inEndTransition)
		{
			return;
		}
		else
		{
			inEndTransition = true;
		}
		
		if (!dontFadeOnEnd)
		{
			state = ScreenChangerState.OUT;
			time = 0;
			destScreen = screenId;
			
			spriteRenderer.color = new Color(1f, 1f, 1f, 0f);
			collider2D.enabled = true;
		}
		else
		{
			if (!asyncLevelLoad)
			{
				Application.LoadLevel(screenId);
			}
			else
			{
				ChangeAsyncLevel();
			}
		}
	}
	
	public void ChangeAsyncLevel()
	{
		asyncOp.allowSceneActivation = true;
	}
	
	private IEnumerator LoadLevelInBackground()
	{
		asyncOp = Application.LoadLevelAsync(asyncLevel);
		asyncOp.allowSceneActivation = false;
		
		yield return asyncOp;
	}
}
