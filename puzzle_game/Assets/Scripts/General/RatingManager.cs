﻿using UnityEngine;

using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;

public class RatingManager : MonoBehaviour 
{
	public static RatingManager instance;
	
	public string testPlayerName;
	public bool setName;
	
	public float sendCheckPeriod;
	
	private string recieveRatingUrl = "http://game.troi.pro/rating/recieve_rating.php";
	//private string updateRatingInfoUrl = "http://game.troi.pro/rating/update_rating_info.php";
	
	private bool sendRatingNextFrame;
	private float time;
	
	void Start() 
	{
		instance = this;
		
		sendRatingNextFrame = true;
		time = 0f;
		
		if (setName)
		{
			PlayerPrefs.SetString("PlayerName", testPlayerName);
		}
	}
	
	void Update() 
	{
		time += Time.deltaTime;
		
		if (sendRatingNextFrame)
		{
			if (PlayerPrefs.HasKey("PlayerName") && PlayerPrefs.HasKey("RatingUpdateNeed"))
			{
				SendRatingValue();
			}
			
			sendRatingNextFrame = false;
			time = 0f;
		}
		
		if (time > sendCheckPeriod)
		{
			sendRatingNextFrame = true;
		}
	}
	
	public void SendRatingValue()
	{
		StartCoroutine("SendRatingInfo");
	}
	
	public IEnumerator SendRatingInfo()
	{
		yield return new WaitForEndOfFrame();
		
		WWWForm form = new WWWForm();
		
		form.AddField("app_id", PlayerPrefs.GetString("AppInstanceId"));
		form.AddField("points", PlayerPrefs.GetInt("Points"));
		form.AddField("points_code", GeneratePointsCode(PlayerPrefs.GetInt("Points")));
		
		WWW www = new WWW(recieveRatingUrl, form);
		yield return www;
		
		if (www.error != null)
		{
			if (PlayerPrefs.HasKey("RatingUpdateNeed"))
			{
				PlayerPrefs.DeleteKey("RatingUpdateNeed");
			}
		}
		else
		{
			PlayerPrefs.SetInt("RatingUpdateNeed", 1);
		}
	}
	
	private string GeneratePointsCode(int points)
	{
		Dictionary<char, string> encodeMapping = new Dictionary<char, string>();
		
		encodeMapping.Add('0', "e");
		encodeMapping.Add('1', "4");
		encodeMapping.Add('2', "7");
		encodeMapping.Add('3', "f");
		encodeMapping.Add('4', "a");
		encodeMapping.Add('5', "8");
		encodeMapping.Add('6', "0");
		encodeMapping.Add('7', "2");
		encodeMapping.Add('8', "c");
		encodeMapping.Add('9', "1");
		encodeMapping.Add('a', "3");
		encodeMapping.Add('b', "9");
		encodeMapping.Add('c', "b");
		encodeMapping.Add('d', "5");
		encodeMapping.Add('e', "d");
		encodeMapping.Add('f', "6");
	
		string hashString = points.ToString();
	
		for (int i = 0; i < 3; i++)	
		{
			hashString = CalcMd5Hash(hashString);
		}
		
		string resultString = "";
		
		for (int i = 0; i < hashString.Length; i++)
		{
			resultString += encodeMapping[hashString[i]];
		}
		
		return resultString;
	}
	
	public string CalcMd5Hash(string value)
	{
		MD5 md5Hasher = MD5.Create();
		
		byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(value));
		
		StringBuilder sBuilder = new StringBuilder();
		
		for (int i = 0; i < data.Length; i++)
		{
			sBuilder.Append(data[i].ToString("x2"));
		}
		
		Debug.Log("Calc hash. Value: " + value + " ; Result: " + sBuilder.ToString());
		
		return sBuilder.ToString();
	}
}
