﻿using UnityEngine;

using System.Collections;

public class StateManager : MonoBehaviour 
{
	public static StateManager instance;
	
	public bool clearPrefs = false;
	public int startLevel = -1;
	
	void Awake()
	{
		if (!PlayerPrefs.HasKey("AppInstanceId"))
		{
			PlayerPrefs.SetString("AppInstanceId", GenerateAppInstanceId());
		}
		
		if (!PlayerPrefs.HasKey("CurrentLevel"))
		{
			PlayerPrefs.SetInt("CurrentLevel", 0);
		}
		
		if (!PlayerPrefs.HasKey("PuzzlePackIndex"))
		{
			PlayerPrefs.SetInt("PuzzlePackIndex", -1);
		}
	}

	void Start() 
	{
		instance = this;
		
		if (clearPrefs)
		{
			PlayerPrefs.DeleteAll();
		}
		
		CheckFirstState();
		ApplyPrefs();
		
		if (startLevel != -1)
		{
			Debug.Log("Override current level");
			//if (!PlayerPrefs.HasKey("CurrentLevel"))
			//{
			PlayerPrefs.SetInt("CurrentLevel", startLevel);
			//}
		}
		
		Debug.Log("App instance id: " + PlayerPrefs.GetString("AppInstanceId"));
	}
	
	private void CheckFirstState()
	{
		if (!PlayerPrefs.HasKey("CurrentTime"))
		{
			PlayerPrefs.SetInt("CurrentTime", 3601);
		}
		
		if (!PlayerPrefs.HasKey("Points"))
		{
			PlayerPrefs.SetInt("Points", 0);
		}
		
		if (!PlayerPrefs.HasKey("HintCount"))
		{
			PlayerPrefs.SetInt("HintCount", 30);
		}
		
		if (!PlayerPrefs.HasKey("PuzzlePackIndex"))
		{
			PlayerPrefs.SetInt("PuzzlePackIndex", -1);
		}
		
		if (!PlayerPrefs.HasKey("SoundEnabled"))
		{
			PlayerPrefs.SetInt("SoundEnabled", 1);
		}
		
		if (!PlayerPrefs.HasKey("MusicEnabled"))
		{
			PlayerPrefs.SetInt("MusicEnabled", 1);
		}
	}
	
	private void ApplyPrefs()
	{
		if (PlayerPrefs.GetInt("SoundEnabled") == 1)
		{
			GameSettings.soundEnabled = true;
		}
		else
		{
			GameSettings.soundEnabled = false;
		}
		
		if (PlayerPrefs.GetInt("MusicEnabled") == 1)
		{
			GameSettings.musicEnabled = true;
		}
		else
		{
			GameSettings.musicEnabled = false;
		}
	}

	private string GenerateAppInstanceId()
	{
		string alphabet = "abcdefghijklmnopqrtstuvwxyzABCDEFGHIJKLMNOPQRTUVWXYZ0123456789";
		int len = 20;
		string result = "";

		for (int i = 0; i < len; i++)
		{
			result += alphabet[Random.Range(0, alphabet.Length)];
		}

		return result;
	}
}
