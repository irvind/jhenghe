﻿using UnityEngine;
using System.Collections;

public class CameraBounds : MonoBehaviour 
{
	public float Top
	{
		get
		{
			return camera.orthographicSize + transform.position.y;
		}
	}

	public float Bottom
	{
		get
		{
			return -camera.orthographicSize + transform.position.y;
		}
	}

	public float Left
	{
		get
		{
			float aspectRatio = (float)Screen.width / (float)Screen.height;

			return -camera.orthographicSize * aspectRatio + transform.position.x;
		}
	}

	public float Right
	{
		get
		{
			float aspectRatio = (float)Screen.width / (float)Screen.height;
			
			return camera.orthographicSize * aspectRatio + transform.position.x;
		}
	}
}
