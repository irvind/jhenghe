﻿using UnityEngine;
using System.Collections;

public class QuestionButton : MonoBehaviour 
{
	public SpriteRenderer spriteRenderer;
	public ScreenShading screenShading;
	
	//private GameController gameController;

	//private Vector3 sourcePosition;
	//private Vector3 clickPosition;

	//private bool hiddenNumsActive;

	//private Calculator calculator;
	//private HintButton hintButton;

	void Start() 
	{
		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		
		Vector3 pos = transform.position;
		pos.x = cameraBounds.Right + 0.01f;
		pos.y = cameraBounds.Bottom - 0.01f;
		transform.position = pos;

		spriteRenderer = GetComponent<SpriteRenderer>();
		//gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

		//sourcePosition = transform.position;
		//clickPosition = new Vector3(sourcePosition.x + 0.02f, sourcePosition.y - 0.02f,
		//    sourcePosition.z);
		    
		//hiddenNumsActive = false;

		//calculator = GameObject.FindGameObjectWithTag("Calculator").GetComponent<Calculator>();
		//hintButton = GameObject.Find("hint_button").GetComponent<HintButton>();
	}

	void OnMouseDown()
	{
		/*
		if (gameController.gameEnding || calculator.calcActive
		    || (screenShading.ShadingOn && screenShading.ShadingMode != ShadingMode.QUESTION_BUTTON_SHADING))
		{
			return;
		}

		SoundManager.instance.PlayMenuClickSound();

		spriteRenderer.color = new Color32(155, 155, 155, 255);
		transform.position = clickPosition;
		*/
	}

	void OnMouseUp()
	{
		/*
		if (gameController.gameEnding || calculator.calcActive
		    || (screenShading.ShadingOn && screenShading.ShadingMode != ShadingMode.QUESTION_BUTTON_SHADING))
		{
			return;
		}
		
		spriteRenderer.color = new Color32(255, 255, 255, 255);
		transform.position = sourcePosition;
		
		hiddenNumsActive = !hiddenNumsActive;
		
		if (hiddenNumsActive)
		{
			ActivateHiddenNumScreen();
		}
		else
		{
			DeactivateHiddenNumScreen();
		}
		*/
	}
	
	public void ActivateHiddenNumScreen()
	{
		/*
		SetQuestionScreen(true);
		
		gameController.DeselectFigure();
		screenShading.SetShading(ShadingMode.QUESTION_BUTTON_SHADING);
		*/
	}
	
	public void DeactivateHiddenNumScreen()
	{
		SetQuestionScreen(false);
		
		screenShading.ClearShading();
	}

	private void SetQuestionScreen(bool newState)
	{
		Vector3 pos;

		if (newState)
		{
			pos = new Vector3(0f, 0f, -0.5f);
		}
		else
		{
			pos = new Vector3(0f, 0f, 900f);
		}

		GameObject[] containers = GameObject.FindGameObjectsWithTag("HiddenNumContainer");
		foreach (GameObject obj in containers)
		{
			obj.transform.localPosition = pos;
		}
	}
}
