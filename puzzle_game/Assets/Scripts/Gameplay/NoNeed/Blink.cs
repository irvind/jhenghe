using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour 
{
	public Figure figure;
	public Sprite sprite;
	public Sprite selectedSprite;
	public bool isReset;
	public bool isInstant = false;

	public float animLenght = 0.75f;

	private float animTime;
	private bool spriteSet;
	private float halfAnimLenght;

	void Start() 
	{
		animTime = 0f;
		spriteSet = false;
		halfAnimLenght = animLenght / 2;
	}

	void Update() 
	{
		if (isInstant)
		{
			if (isReset)
			{
				//figure.ResetSprite();
			}
			else
			{
				//figure.SetSprite(sprite, selectedSprite);
			}
			
			Destroy(gameObject);
			return;
		}
	
		animTime += Time.deltaTime;

		Vector3 fromVec;
		Vector3 toVec;
		float t;

		if (animTime < halfAnimLenght)
		{
			fromVec = new Vector3(1, 1, 1);
			toVec = new Vector3(0.1f, 0.1f, 0.1f);
			t = animTime / halfAnimLenght;
		}
		else
		{
			if (animTime > animLenght)
			{
				figure.transform.localScale = new Vector3(1, 1, 1);
				Destroy(gameObject);
				return;
			}

			if (!spriteSet)
			{
				if (isReset)
				{
					//figure.ResetSprite();
				}
				else
				{
					//figure.SetSprite(sprite, selectedSprite);
				}

				spriteSet = true;
			}

			fromVec = new Vector3(0.1f, 0.1f, 0.1f);
			toVec = new Vector3(1, 1, 1);
			t = (animTime - halfAnimLenght) / halfAnimLenght;
		}

		figure.transform.localScale = Vector3.Lerp(fromVec, toVec, t);
	}
}
