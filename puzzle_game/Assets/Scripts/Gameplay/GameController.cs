using UnityEngine;

using System;
using System.Text;
using System.Collections;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

public class GameController : MonoBehaviour 
{
	public bool gameStarted;
	public bool gameEnding;
	public bool puzzleInitialized;

	public Sprite[] darkFiguresSprites;
	public Sprite[] orangefiguresSprites;

	public Sprite[] darkNumbersSprites;
	public Sprite[] orangeNumbersSprites;

	public List<int>[] suggestions;

	public Puzzle currentPuzzle;
	
	public bool breakTimeOnHint = true;

	private Figure selectedFigure;

	private PuzzleInitializer puzzleInitializer;
	private GameplayTimer gameplayTimer;
	private Calculator calculator;
	private BottomPanel bottomPanel;
	private ScreenChanger screenChanger;
	
	private Figure changingFigure;
	public bool changindMode;
	private int changingIndex;

	private List<List<int>[]> suggestionsHistory;
	private int historyIndex;
	
	private TextMesh scoreText;
	public TextMesh addPointsText;
	public float addPointsScaleSpeed;
	
	public ScreenShading screenShading;
	public Transform topPanelTransform;
	
	public GameObject addPuzzlesGroup;
	public GameObject addHintsGroup;

	void Start() 
	{
		puzzleInitializer = GetComponent<PuzzleInitializer>();
		gameplayTimer = GameObject.FindGameObjectWithTag("TimeCounter").GetComponent<GameplayTimer>();
		calculator = GameObject.FindGameObjectWithTag("Calculator").GetComponent<Calculator>();
		bottomPanel = GameObject.FindGameObjectWithTag("BottomPanel").GetComponent<BottomPanel>();
		scoreText = GameObject.FindGameObjectWithTag("Score").GetComponent<TextMesh>();
		screenChanger = GameObject.FindGameObjectWithTag("ScreenChanger").GetComponent<ScreenChanger>();

		gameStarted = false;
		gameEnding = false;
		changindMode = false;
		puzzleInitialized = false;

		// В начале никаких предположений
		suggestions = new List<int>[10];
		for (int i = 0; i < 10; i++)
		{
			suggestions[i] = new List<int>();
		}

		// Инициализируем список истории
		suggestionsHistory = new List<List<int>[]>();
		historyIndex = -1;

		// Ставим сохраненые очки
		scoreText.text = PlayerPrefs.GetInt("Points").ToString();

		// Врубаем звук, если нужно
		if (GameSettings.soundEnabled && GameSettings.musicEnabled)
		{
			audio.Play();
		}
	}

	void Update()
	{
		// Если не инициализировали, инициализируем
		if (!gameStarted)
		{
			if (PuzzleManager.instance.HavePuzzle())
			{
				InitializeGame();
			}
			else
			{
				addPuzzlesGroup.SetActive(true);
			}

			gameStarted = true;
		}

		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			screenChanger.GoToScreen(1);
		}

		// Если сейчас меняются фигурки
		if (changindMode)
		{
			List<Figure> figures = puzzleInitializer.figuresInOrder;

			// Если анимация фигурки закончилась
			if (!figures[changingIndex].InAnimation)
			{
				// ... то берем следуюшую. Если фигурок больше нет
				if (!SelectChangingFigure())
				{
					if (CheckPuzzleCompletion())
					{
						Debug.Log("Puzzle complete");
						if (CheckPuzzle())
						{
							Debug.Log("End game");
							EndGame();
						}
					}
					else
					{
						Debug.Log("Puzzle not complete");
					}
				}
			}
		}

		// Если игра закончилась и таймер отсчитал все время (+ пауза в конце)
		if (gameEnding)
		{
			float val = addPointsText.transform.localScale.x;
			val += Time.deltaTime * addPointsScaleSpeed;
			if (val > 1f)
			{
				addPointsText.transform.localScale = new Vector3(1f, 1f, 1f);
			}
			else
			{
				addPointsText.transform.localScale = new Vector3(val, val, val);
			}
		
			if (gameplayTimer.mode == GameplayTimerMode.END)
			{
				// ... то переходим на экран рекламы
				screenChanger.GoToScreen(3);
			}
		}
	}

	// *****************************************************
	// Загружаем текущий пазл и сохраненое состояние фигурок
	// *****************************************************
	public void InitializeGame()
	{
		currentPuzzle = PuzzleManager.instance.GetPuzzle();
		puzzleInitializer.InitializePuzzle(currentPuzzle);

		List<int>[] s = puzzleInitializer.LoadSavedSuggestions();
		if (s != null)
		{
			suggestions = s;
		}
		
		for (int i = 0; i < currentPuzzle.openedFigures.Length; i++)
		{
			int figTypeInt = currentPuzzle.openedFigures[i];
			int number = currentPuzzle.GetTypeNumber((FigureType)figTypeInt);
			
			suggestions[figTypeInt] = new List<int>();
			suggestions[figTypeInt].Add(number);
		}

		SaveHistoryState(); 
		
		gameStarted = true;
		
		OutputSuggestions();
	}
	
	public void AddScore(int val)
	{
		int newScore = int.Parse(scoreText.text) + val;
		scoreText.text = newScore.ToString();
		
		int newAddScore = int.Parse(addPointsText.text) + val;
		addPointsText.text = "+" + newAddScore.ToString();
	}
	
	public void OnClearNumButtonClick()
	{
		if (gameEnding || calculator.IsEnabled)
		{
			return;
		}
		
		for (int i = 0; i < 10; i++)
		{
			FigureType type = (FigureType)i;
			
			if (Array.IndexOf(currentPuzzle.openedFigures, i) != -1)
			{
				continue;
			}
			
			if (currentPuzzle.HaveFigureType(type))
			{
				ProcessFigures(type, new int[0]);
			}
		}
		
		StartChangingMode();
		
		SaveHistoryState();
		SaveSuggestions();
		
		selectedFigure = null;
		bottomPanel.Deactivate();
	}
	
	public void OnAcceptButtonClick(int[] activeNumbers)
	{
		bottomPanel.Deactivate();
		
		if (gameEnding || calculator.IsEnabled)
		{
			return;
		}
		
		if (changindMode)
		{
			return;
		}

		if (selectedFigure != null)
		{
			ProcessFigures(selectedFigure.Type, activeNumbers);
			StartChangingMode();

			SaveHistoryState();
			SaveSuggestions();

			selectedFigure = null;
			
			OutputSuggestions();
		}
	}
	
	public void OnHintButtonClick(HintButton hintButton)
	{
		if (gameEnding || calculator.IsEnabled)
		{
			return;
		}
		
		if (selectedFigure != null)
		{
			int hintCount = PlayerPrefs.GetInt("HintCount");
			if (hintCount > 0)
			{
				int number = currentPuzzle.GetTypeNumber(selectedFigure.Type);
				//Debug.Log(number.ToString());
				
				ProcessFigures(selectedFigure.Type, new int[] {number});
				StartChangingMode();
				
				SaveHistoryState();
				SaveSuggestions();
				
				selectedFigure = null;
				bottomPanel.Deactivate();
				
				if (breakTimeOnHint)
				{
					gameplayTimer.SetTime(0);
				}
				
				hintCount--;
				hintButton.hintText.text = hintCount.ToString();
				PlayerPrefs.SetInt("HintCount", hintCount);
				
				OutputSuggestions();
			}
			else
			{
				bottomPanel.Deactivate(false);
				addHintsGroup.SetActive(true);
			}
		}
	}

	public void AddHints(int addCount, HintButton hintButton)
	{
		int hintCount = PlayerPrefs.GetInt("HintCount");
		hintCount += addCount;
		PlayerPrefs.SetInt("HintCount", hintCount);
		
		hintButton.hintText.text = hintCount.ToString();
	}

	public void OnHideNumClick()
	{
		List<Figure> figures = puzzleInitializer.figuresInOrder;

		for (int i = 0; i < figures.Count; i++) 
		{
			figures[i].ToogleNums();
		}
	}

	public bool OnFigureClick(Figure figure)
	{
		if (gameEnding || calculator.IsEnabled || bottomPanel.activated)
		{
			return false;
		}

		int[] figureSuggestions = figure.Suggestions;
		
		/*
		string message = "On figure click suggestions: ";
		for (int i = 0; i < figureSuggestions.Length; i++)
		{
			message += figureSuggestions[i].ToString() + " ";
		}
		
		Debug.Log(message);
		*/
		
		FigureType figureType = figure.Type;
		
		GameObject[] objects = GameObject.FindGameObjectsWithTag("PanelNumber");
		PanelNumber[] panelNumbers = new PanelNumber[objects.Length];

		int[] openedNumbers = currentPuzzle.GetOpenedNumbers();

		// Ставим состояние кнопок панели в дефолтное (кроме открытых цифр)
		for (int i = 0; i < objects.Length; i++)
		{
			panelNumbers[i] = objects[i].GetComponent<PanelNumber>();

			if (Array.IndexOf(openedNumbers, panelNumbers[i].number) != -1)
			{
				continue;
			}

			panelNumbers[i].State = PanelNumberState.NORMAL;
		}

		// Блокируем занятые цифры
		for (int i = 0; i < suggestions.Length; i++)
		{
			FigureType tempType = (FigureType)i;
			List<int> sugList = suggestions[i];
			
			if ((sugList.Count == 1) && (tempType != figureType))
			{
				for (int j = 0; j < panelNumbers.Length; j++)
				{
					if (panelNumbers[j].number == sugList[0])
					{
						panelNumbers[j].State = PanelNumberState.LOCKED;
						break;
					}
				}
			}
		}

		// Подсвечиваем уже выбраные цифры
		for (int i = 0; i < figureSuggestions.Length; i++)
		{
			int num = figureSuggestions[i];
			
			if (num == -10)
			{
				num = 0;
			}
			else if (num < 0)
			{
				num = -num;
			}
			
			for (int j = 0; j < panelNumbers.Length; j++)
			{
				if (panelNumbers[j].number == num)		
				{
					panelNumbers[j].State = PanelNumberState.ACTIVE;
				}
			}
		}

		bottomPanel.Activate();

		selectedFigure = figure;

		return true;
	}

	// *****************************************************
	// Начинаем заканчивать игру
	// *****************************************************
	public void EndGame()
	{
		PuzzleManager.instance.NextPuzzle();
		
		int p = int.Parse(scoreText.text);
		PlayerPrefs.SetInt("Points", p + gameplayTimer.GetCurrentPoints());

		PlayerPrefs.DeleteKey("CurrentTime");
		DeleteSavedSuggestions();
		
		gameEnding = true;		
	
		gameplayTimer.Finish();

		addPointsText.gameObject.SetActive(true);
		screenShading.SetShading(ShadingMode.BOTTOM_PANEL_SHADING);
		
		Vector3 pos = topPanelTransform.position;
		pos.z = -3f;
		topPanelTransform.position = pos;
		
		StatisticsManager.instance.AddPuzzleSolve();
	}

	// *****************************************************
	// Обновляем состояние фигурок при новых предположениях
	// *****************************************************
	private void ProcessFigures(FigureType figureType, int[] newSuggestion, bool stayUnset = false)
	{
		int figTypeNum = (int)figureType;

		// Обновляем (отложенно)
		Figure[] figures = GetAllFiguresByType(figureType);
		for (int i = 0; i < figures.Length; i++)
		{
			figures[i].SetDelayedSuggestions((int[])newSuggestion.Clone(), false);
		}

		// Сохраняем в глобальных предположениях
		List<int> globalSug = suggestions[figTypeNum];
		globalSug.Clear();
		globalSug.AddRange(newSuggestion);

		if ((newSuggestion.Length == 1) && (newSuggestion[0] >= 0))
		{
			for (int i = 0; i < 10; i++)
			{
				if (i == figTypeNum)
				{
					continue;
				}

				List<int> s = suggestions[i];
				int index = s.IndexOf(newSuggestion[0]);
				if (index == -1)
				{
					continue;
				}

				s.RemoveAt(index);
				
				if (s.Count == 1)
				{
					if (s[0] == 0)
					{
						s[0] = -10;
					}
					else
					{
						s[0] = -s[0];
					}
				}
				
				ProcessFigures((FigureType)i, s.ToArray());
			}
		}
	}
	
	private void OutputSuggestions()
	{
		StringBuilder strBuilder = new StringBuilder("Suggestions: ");
	
		for (int i = 0; i < 10; i++)
		{
			List<int> l = suggestions[i];
			strBuilder.Append(i + ": ");
			
			if (l.Count != 0)
			{
				for (int j = 0; j < l.Count; j++)
				{
					strBuilder.Append(l[j] + " ");
				}
			}
			else
			{
				strBuilder.Append("no ");
			}
			
			strBuilder.Append(" ; ");
		}
		
		Debug.Log(strBuilder.ToString());
	}

	// *****************************************************
	// Проверить: выставленны ли все цифры
	// *****************************************************
	private bool CheckPuzzleCompletion()
	{
		for (int i = 0; i < 10; i++)
		{
			List<int> suggestionList = suggestions[i];
			bool figureExsist = currentPuzzle.HaveFigureType((FigureType)i);

			if (figureExsist && ((suggestionList.Count != 1) || (suggestionList.Count == 1 && suggestionList[0] < 0)))
			{
				string message = string.Format("Puzzle completion failed {0}. {1} {2}", i, figureExsist,
				    suggestionList.Count);
				                           
				Debug.Log(message);
				return false;
			}
		}

		return true;
	}
	
	private bool CheckPuzzle()
	{
		bool solved = true;

		for (int i = 0; i < 10; i++)
		{
			FigureType type = (FigureType)i;
			List<int> suggestionList = suggestions[i];

			int typeNumber = currentPuzzle.GetTypeNumber(type);

			if ((suggestionList.Count == 1) && (suggestionList[0] != typeNumber))
			{
				Debug.Log(string.Format("suggestionList[0] = {0} ; typeNumber = {1}", suggestionList[0], typeNumber));
				solved = false;
				break;
			}
		}

		return solved;
	}
	
	private void SaveHistoryState()
	{
		List<int>[] curStateCopy = new List<int>[10];
		for (int i = 0; i < 10; i++)
		{
			curStateCopy[i] = new List<int>();
			curStateCopy[i].AddRange(suggestions[i]);
		}

		if (historyIndex < suggestionsHistory.Count - 1)
		{
			suggestionsHistory.RemoveRange(historyIndex + 1, suggestionsHistory.Count - (historyIndex + 1));
		}
		
		suggestionsHistory.Add(curStateCopy);
		historyIndex++;
	}
	
	public void SaveSuggestions()
	{
		BinaryFormatter binaryFormatter = new BinaryFormatter();
		FileStream file = File.Create(Application.persistentDataPath + "/curState.dat");
		
		binaryFormatter.Serialize(file, suggestions);
		file.Close();
	}

	private void DeleteSavedSuggestions()
	{
		string path = Application.persistentDataPath + "/curState.dat";
		if(File.Exists(path))
		{
			File.Delete(path);
		}
	}

	private void SetSuggestionsState(List<int>[] newState)
	{
		bool fl = false;

		for (int i = 0; i < 10; i++)
		{
			if (!suggestions[i].SequenceEqual(newState[i]))
			{
				fl = true;
				ProcessFigures((FigureType)i, newState[i].ToArray());
			}
		}

		if (fl)
		{
			StartChangingMode();
		}
	}
	
	public void LoadPreviousState()
	{
		if (historyIndex > 0)
		{
			historyIndex--;
			SetSuggestionsState(suggestionsHistory[historyIndex]);
		}
		
		SaveSuggestions();
	}
	
	public void LoadForwardState()
	{
		if (historyIndex < suggestionsHistory.Count - 1)
		{
			historyIndex++;
			SetSuggestionsState(suggestionsHistory[historyIndex]);
		}
		
		SaveSuggestions();
	}

	private Figure[] GetAllFiguresByType(FigureType figType)
	{
		string tagName = "Figure" + (int)figType;
		GameObject[] figObjs = GameObject.FindGameObjectsWithTag(tagName);

		Figure[] figures = new Figure[figObjs.Length];
		for (int i = 0; i < figures.Length; i++)
		{
			figures[i] = figObjs[i].GetComponent<Figure>();
		}

		return figures;
	}

	private void StartChangingMode()
	{
		changindMode = true;
		changingIndex = -1;

		SelectChangingFigure();
	}

	private bool SelectChangingFigure()
	{
		List<Figure> figures = puzzleInitializer.figuresInOrder;

		changingIndex++;
		while(changingIndex < figures.Count)
		{
			if (figures[changingIndex].ApplyDelayedSuggestions() == false)
			{
				changingIndex++;
			}
			else
			{
				break;
			}
		}
		
		if (changingIndex >= figures.Count)
		{
			changindMode = false;
			return false;
		}
		else
		{
			return true;
		}
	}

	public void StartShowPuzzleCoroutine()
	{
		StartCoroutine(ShowPuzzleCoroutine());
	}

	public IEnumerator ShowPuzzleCoroutine()
	{
		InitializeGame();
		GameObject puzzleObj = GameObject.Find("Current_puzzle");
		puzzleObj.SetActive(false);
		
		yield return new WaitForSeconds(0.3f);
		
		puzzleObj.SetActive(true);
		
		float curPuzzleScale = 0.05f;
		float puzzleScaleSpeed = 2f;
		
		while (true)
		{
			curPuzzleScale += puzzleScaleSpeed * Time.deltaTime;
			
			if (curPuzzleScale < 1f)
			{
				Vector3 scaleVec = new Vector3(curPuzzleScale, curPuzzleScale, 1f);
				puzzleObj.transform.localScale = scaleVec;
				
				yield return null;
			}
			else
			{
				puzzleObj.transform.localScale = new Vector3(1f, 1f, 1f);
				break;
			}
		}
	}
}
