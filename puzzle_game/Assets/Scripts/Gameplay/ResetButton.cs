﻿using UnityEngine;
using System.Collections;

public class ResetButton : MonoBehaviour 
{
	public bool isHiding;

	public Transform otherTransform;

	private GameController gameController;

	void Start() 
	{
		isHiding = false;

		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}
	
	void OnMouseUp()
	{
		PuzzleManager.instance.ResetPuzzles();
		StartCoroutine(HideButtons());
	}

	private IEnumerator HideButtons()
	{
		float curScale = 0.75f;
		float scaleSpeed = 3f;

		while (true)
		{
			curScale -= scaleSpeed * Time.deltaTime;

			if (curScale > 0.1f)
			{
				Vector3 scaleVec = new Vector3(curScale, curScale, 1f);

				transform.localScale = scaleVec;
				otherTransform.localScale = scaleVec;

				yield return null;
			}
			else
			{
				transform.localScale = new Vector3(1f, 1f, 1f);
				otherTransform.localScale = new Vector3(1f, 1f, 1f);

				break;
			}
		}

		gameController.StartShowPuzzleCoroutine();
		transform.parent.gameObject.SetActive(false);
	}
}
