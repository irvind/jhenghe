﻿using UnityEngine;
using System.Collections;

public class BottomLine : MonoBehaviour 
{
	void Start () 
	{
		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		
		Vector3 pos = transform.position;
		pos.y = cameraBounds.Bottom - 0.01f;
		transform.position = pos;
	}
}
