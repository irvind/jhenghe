using UnityEngine;

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;

[System.Serializable]
public class SignSprites
{
	public Sprite plus;
	public Sprite minus;
	public Sprite multiply;
	public Sprite division;
	public Sprite equals;
	
	public Sprite GetSignSprite(string sign)
	{
		if (sign == "+")
		{
			return plus;
		}
		else if (sign == "-")
		{
			return minus;
		}
		else if (sign == "*")
		{
			return multiply;
		}
		else if (sign == "/")
		{
			return division;
		}
		else if (sign == "=") 
		{
			return equals;
		}
		else 
		{
			return null;
		}
	}
}

public class PuzzleInitializer : MonoBehaviour 
{
	public GameObject puzzleContainerPrefab;
	public GameObject figurePrefab;
	public GameObject signPrefab;
	public SignSprites signSprites;

	public Vector3 puzzlePosition;
	public List<Figure> figuresInOrder;
	
	private GetPuzzleGroups puzzleGroups;
	
	void Start() 
	{
		figuresInOrder = new List<Figure>();
	}

	public void InitializePuzzle(Puzzle puzzle)
	{
		GameObject obj = (GameObject)Instantiate(puzzleContainerPrefab, Vector3.zero, Quaternion.identity);
		puzzleGroups = obj.GetComponent<GetPuzzleGroups>();
		obj.name = "Current_puzzle";
		obj.transform.position = puzzlePosition;
		
		if (!puzzle.isSimple)
		{
			InitializePuzzleFigures(puzzle);
			InitializePuzzleOperations(puzzle);
			AdjustPosition(puzzleGroups, obj);
		}
		else
		{
			Debug.Log("Init simple puzzle");
			
			Destroy(obj.transform.Find("pc_row4/horiz_line").gameObject);
			
			InitializeSimplePuzzleFigures(puzzle);
			InitializeSimplePuzzleOperations();
			AdjustSimplePuzzlePosition(obj);
		}
		
		SetFiguresInidices();	
	}
	
	private void AdjustPosition(GetPuzzleGroups puzzleGroups, GameObject puzzleObj)
	{
		Bounds bounds = new Bounds();
		
		for (int i = 0; i < 5; i += 2)
		{
			foreach (Transform t in puzzleGroups.groups[0][i])
			{
				bounds.Encapsulate(t.GetComponent<SpriteRenderer>().bounds);
			}
			
			foreach (Transform t in puzzleGroups.groups[4][i])
			{
				bounds.Encapsulate(t.GetComponent<SpriteRenderer>().bounds);
			}
		}
		
		Vector3 vec = puzzleObj.transform.position;
		vec.x = -bounds.center.x;
		puzzleObj.transform.position = vec;
	}

	private void InitializePuzzleFigures(Puzzle puzzle)
	{
		AdjustNumberGroups(puzzle);

		float curZ = 0;
		float zStep = -0.01f;
		List<int>[] loadedSug = LoadSavedSuggestions();

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				curZ = InitializeFigureGroup(puzzle, i, j, curZ, zStep, loadedSug);
			}
		}
	}

	private void AdjustNumberGroups(Puzzle puzzle)
	{
		int[] maxLengths = new int[3] {0, 0, 0};
		float x = 0;
		
		for (int i = 1; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				string number = puzzle.numbers[j * 3 + i];
				if (number.Length > maxLengths[i])
				{
					maxLengths[i] = number.Length;
				}
			}
			
			x += 2 + (maxLengths[i] - 1) * 1.3f;
			
			for (int j = 0; j < 3; j++)
			{
				puzzleGroups.groups[j * 2][i * 2].localPosition = new Vector3(x, 0f, 0f);
			}
		}
	}

	private float InitializeFigureGroup(Puzzle puzzle, int colIndex, int rowIndex, float zVal, float zStep, List<int>[] savedSug)
	{
		Transform container = puzzleGroups.groups[rowIndex * 2][colIndex * 2];
		string numberStr = puzzle.numbers[rowIndex * 3 + colIndex];
		
		for (int k = 0; k < numberStr.Length; k++)
		{
			GameObject figObj = (GameObject)Instantiate(figurePrefab, Vector3.zero, Quaternion.identity);
			Figure figure = figObj.GetComponent<Figure>();
			
			int number = int.Parse(numberStr.Substring(k, 1));
			FigureType figureType = puzzle.GetNumberType(number);
			
			int[] suggestions = null;
			bool isOpened;
			if (Array.IndexOf(puzzle.openedFigures, (int)figureType) != -1)
			{
				isOpened = true;
				suggestions = new int[1] {number};
			}
			else
			{
				isOpened = false;

				if (savedSug != null)
				{
					suggestions = savedSug[(int)figureType].ToArray();
				}
				else
				{
					suggestions = new int[0];
				}
			}
			
			figure.Initialize(figureType, suggestions, isOpened);
			figure.tag = "Figure" + (int)figureType;
			figure.transform.parent = container;
			figure.transform.localPosition = new Vector3((numberStr.Length - 1) * -1.3f + k * 1.3f, 0f, zVal);
		
			zVal += zStep;
		}

		return zVal;
	}

	public List<int>[] LoadSavedSuggestions()
	{
		if (File.Exists(Application.persistentDataPath + "/curState.dat"))
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			FileStream file = File.Open(Application.persistentDataPath + "/curState.dat", FileMode.Open);
			
			List<int>[] loadedSug = (List<int>[])binaryFormatter.Deserialize(file);

			file.Close();

			return loadedSug;
		}

		return null;
	}

	private void InitializePuzzleOperations(Puzzle puzzle)
	{
		AdjustOperations();

		for (int i = 0; i < 3; i++)
		{
			Transform container = puzzleGroups.groups[i * 2][1];
			string operation = puzzle.horOperations[i];
			Sprite sprite = signSprites.GetSignSprite(operation);
			
			GameObject sign = (GameObject)Instantiate(signPrefab, Vector3.zero, Quaternion.identity);
			sign.GetComponent<Sign>().Initialize(operation, sprite);
			sign.transform.parent = container;
			sign.transform.localPosition = new Vector3(0f, 0f, 0f);
		}
		
		for (int i = 0; i < 3; i++)
		{
			Transform container = puzzleGroups.groups[i * 2][3];
			
			GameObject sign = (GameObject)Instantiate(signPrefab, Vector3.zero, Quaternion.identity);
			sign.GetComponent<Sign>().Initialize("=", signSprites.equals);
			sign.transform.parent = container;
			sign.transform.localPosition = new Vector3(0f, 0f, 0f);
		}

		for (int i = 0; i < 3; i++)
		{
			Transform signContainer = puzzleGroups.groups[1][i];
			string operation = puzzle.vertOperations[i];
			Sprite sprite = signSprites.GetSignSprite(operation);
			
			GameObject sign = (GameObject)Instantiate(signPrefab, Vector3.zero, Quaternion.identity);
			sign.GetComponent<Sign>().Initialize(operation, sprite);
			sign.transform.parent = signContainer;
			sign.transform.localPosition = new Vector3(0f, 0f, 0f);
		}
	}

	private void AdjustOperations()
	{
		float x = puzzleGroups.groups[0][2].localPosition.x + 1;
		
		for (int i = 0; i < 3; i++)
		{
			Transform container = puzzleGroups.groups[i * 2][3];
			container.localPosition = new Vector3(x, 0, 0);
		}
		
		for (int i = 0; i < 3; i++)
		{
			Transform container = puzzleGroups.groups[1][i];
			
			x = puzzleGroups.groups[0][i * 2].localPosition.x;
			container.localPosition = new Vector3(x, 0f ,0f);
		}
	}

	private void SetFiguresInidices()
	{
		int curIndex = 0;

		for (int i = 0; i < 3; i++)
		{
			for (int j = 0; j < 3; j++)
			{
				Transform groupTrans = puzzleGroups.groups[i * 2][j * 2];
				foreach (Transform child in groupTrans)
				{
					Figure figure = child.gameObject.GetComponent<Figure>();
					figure.index = curIndex++;
					figuresInOrder.Add(figure);
				}
			}
		}
	}
	
	private void InitializeSimplePuzzleFigures(Puzzle puzzle)
	{
		puzzleGroups.groups[2][2].transform.localPosition = new Vector3(3.3f, 0f, 0f);
		puzzleGroups.groups[2][4].transform.localPosition = new Vector3(7.9f, 0f, 0f);
	
		List<int>[] loadedSug = LoadSavedSuggestions();
	
		string[] figureGroups = new string[] {"50", "50", "100"};
		
		Dictionary<int, FigureType> figurePairs = new Dictionary<int, FigureType>();
		figurePairs.Add(5, FigureType.TYPE_0);
		figurePairs.Add(0, FigureType.TYPE_1);
		figurePairs.Add(1, FigureType.TYPE_2);
		
		float zVal = 0f;
		float zStep = 0.01f;
		
		for (int i = 0; i < 3; i++)
		{
			string group = figureGroups[i];
			Transform container = puzzleGroups.groups[2][i * 2];
			
			for (int j = 0; j < group.Length; j++)
			{
				GameObject figObj = (GameObject)Instantiate(figurePrefab, Vector3.zero, Quaternion.identity);
				Figure figure = figObj.GetComponent<Figure>();
				
				int number = int.Parse(group.Substring(j, 1));
				FigureType figureType = figurePairs[number];
				
				int[] suggestions = null;
				bool opened;
				
				if (Array.IndexOf(puzzle.openedFigures, (int)figureType) != -1)
				{
					opened = true;
					
					suggestions = new int[1] {number};
					Debug.Log("test: " + number.ToString());
				}
				else
				{
					opened = false;
				
					if (loadedSug != null)
					{
						suggestions = loadedSug[(int)figureType].ToArray();
					}
					else
					{
						suggestions = new int[0];
					}
				}
				
				figure.Initialize(figureType, suggestions, opened);
				figure.tag = "Figure" + (int)figureType;
				figure.transform.parent = container;
				figure.transform.localPosition = new Vector3((group.Length - 1) * -1.3f + j * 1.3f, 0f, -zVal);
				
				zVal += zStep;
			}
		}
	}
	
	private void InitializeSimplePuzzleOperations()
	{
		puzzleGroups.groups[2][1].transform.localPosition = new Vector3(1f, 0f, 0f);
		puzzleGroups.groups[2][3].transform.localPosition = new Vector3(4.3f, 0f, 0f);
		
		string[] operations = {"+", "="};
		
		for (int i = 0; i < 2; i++)
		{
			Transform container = puzzleGroups.groups[2][1 + i * 2];
			string operation = operations[i];
			Sprite sprite = signSprites.GetSignSprite(operation);
			
			GameObject sign = (GameObject)Instantiate(signPrefab, Vector3.zero, Quaternion.identity);
			sign.GetComponent<Sign>().Initialize(operation, sprite);
			sign.transform.parent = container;
			sign.transform.localPosition = new Vector3(0f, 0f, 0f);
		}
	}
	
	private void AdjustSimplePuzzlePosition(GameObject obj)
	{
		Bounds bounds = new Bounds();
		
		for (int i = 0; i < 5; i += 2)
		{
			foreach (Transform t in puzzleGroups.groups[2][i])
			{
				bounds.Encapsulate(t.GetComponent<SpriteRenderer>().bounds);
			}
		}
		
		Vector3 vec = obj.transform.position;
		vec.x = -bounds.center.x;
		obj.transform.position = vec;
	}
}
