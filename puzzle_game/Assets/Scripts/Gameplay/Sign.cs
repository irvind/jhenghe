﻿using UnityEngine;
using System.Collections;

public class Sign : MonoBehaviour 
{
	private SpriteRenderer spriteRenderer;
	private string signStr;

	public string SignStr
	{
		get
		{
			return signStr;
		}
	}
	
	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	public void Initialize(string sign, Sprite sprite)
	{
		this.signStr = sign;
		spriteRenderer.sprite = sprite;
	}
}
