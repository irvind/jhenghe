﻿using UnityEngine;
using System.Collections;

public class DownloadPuzzlesButton : MonoBehaviour 
{	
	private GameController gameController;

	public Transform otherTransform;

	private bool waitForDownload;

	void Start() 
	{
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		
		waitForDownload = false;
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}

	void OnMouseUp()
	{
		PuzzleManager.instance.DownloadPuzzlePack();
		waitForDownload = true;
	}

	void Update()
	{
		if (waitForDownload)
		{
			PuzzleManager puzzleManager = PuzzleManager.instance;

			if (puzzleManager.downloadFinished)
			{
				Debug.Log("Download finished");
				Debug.Log("Download status: " + puzzleManager.downloadStatus.ToString());
				
				if (puzzleManager.downloadStatus)
				{
					//puzzleManager.NextPuzzle();
					puzzleManager.LoadCurrentPack();
					Debug.Log(PlayerPrefs.GetInt("CurrentLevel").ToString());
					
					StartCoroutine(HideButtons());
				}
				else
				{
					// show message
				}

				waitForDownload = false;
			}
		}
	}
	
	private IEnumerator HideButtons()
	{
		float curScale = 0.75f;
		float scaleSpeed = 3f;
		
		while (true)
		{
			curScale -= scaleSpeed * Time.deltaTime;
			
			if (curScale > 0.1f)
			{
				Vector3 scaleVec = new Vector3(curScale, curScale, 1f);
				
				transform.localScale = scaleVec;
				otherTransform.localScale = scaleVec;
				
				yield return null;
			}
			else
			{
				transform.localScale = new Vector3(1f, 1f, 1f);
				otherTransform.localScale = new Vector3(1f, 1f, 1f);
								
				break;
			}
		}

		Debug.Log("Start show puzzle");
		gameController.StartShowPuzzleCoroutine();
		transform.parent.gameObject.SetActive(false);
	}
}
