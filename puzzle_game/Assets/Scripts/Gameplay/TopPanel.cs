﻿using UnityEngine;
using System.Collections;

public class TopPanel : MonoBehaviour 
{
	void Start() 
	{
		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		
		Vector3 pos = transform.position;
		pos.y = cameraBounds.Top + 0.02f;
		transform.position = pos;
	}
}
