﻿using UnityEngine;
using System.Collections;

public class MenuText : MonoBehaviour 
{
	private ScreenChanger screenChanger;
	private SpriteRenderer spriteRenderer;

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		
		screenChanger = GameObject.FindGameObjectWithTag("ScreenChanger").GetComponent<ScreenChanger>();
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();

		spriteRenderer.color = new Color32(155, 155, 155, 255);
	}

	void OnMouseUp()
	{
		spriteRenderer.color = new Color32(255, 255, 255, 255);

		screenChanger.GoToScreen(1);
		//Application.LoadLevel(1);
	}
}
