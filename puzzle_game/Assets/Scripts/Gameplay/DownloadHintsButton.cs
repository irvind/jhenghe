﻿using UnityEngine;
using System.Collections;

public class DownloadHintsButton : MonoBehaviour 
{
	public ScreenShading screenShading;
	public HintButton hintButton;
	
	public int hintCount = 100;

	private GameController gameController;
	private string statUrl = "http://game.troi.pro/control_panel/view_stat.php";

	void Start() 
	{
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}

	void OnMouseUp()
	{
		StartCoroutine(DownloadStatPage());
	}

	private IEnumerator DownloadStatPage()
	{
		WWW www = new WWW(statUrl);
		yield return www;

		if (www.error == null)
		{
			gameController.AddHints(hintCount, hintButton);
		}
		else
		{
			// show error
		}

		yield return new WaitForSeconds(0.5f);

		StartCoroutine(HideButton());
	}

	private IEnumerator HideButton()
	{
		float curScale = 0.75f;
		float scaleSpeed = 3f;
		
		while (true)
		{
			curScale -= scaleSpeed * Time.deltaTime;
			
			if (curScale > 0.1f)
			{
				Vector3 scaleVec = new Vector3(curScale, curScale, 1f);
				transform.localScale = scaleVec;
				yield return null;
			}
			else
			{
				transform.localScale = new Vector3(1f, 1f, 1f);
				
				break;
			}
		}
		
		screenShading.ClearShading();
		transform.parent.gameObject.SetActive(false);
	}
}
