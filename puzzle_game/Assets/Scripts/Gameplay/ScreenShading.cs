﻿using UnityEngine;
using System.Collections;

public enum ShadingMode 
{
	CALCULATOR_SHADING, BOTTOM_PANEL_SHADING/*, QUESTION_BUTTON_SHADING*/, NONE
}

public class ScreenShading : MonoBehaviour 
{
	public HintButton hintButton;
	//public QuestionButton questionButton;
	public CalculatorUpArrow upArrowButton;
	
	public byte alpha = 115;
	public Color32 buttonShadingColor;
	
	private SpriteRenderer spriteRenderer;
	private ShadingMode shadingMode;
	
	public ShadingMode ShadingMode
	{
		get
		{
			return shadingMode;
		}
	}
	
	public bool ShadingOn
	{
		get
		{
			return shadingMode != ShadingMode.NONE;
		}
	}

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		shadingMode = ShadingMode.NONE;
	}
	
	public void SetShading(ShadingMode mode)
	{
		if (mode == ShadingMode.NONE)
		{
			return;
		}
		
		shadingMode = mode;
		spriteRenderer.color = new Color32(255, 255, 255, alpha);
	
		if (mode == ShadingMode.CALCULATOR_SHADING)
		{
			hintButton.spriteRenderer.color = buttonShadingColor;
			//questionButton.spriteRenderer.color = buttonShadingColor;
		}
		else if (mode == ShadingMode.BOTTOM_PANEL_SHADING)
		{
			//hintButton.spriteRenderer.color = buttonShadingColor;
			//questionButton.spriteRenderer.color = buttonShadingColor;
			upArrowButton.spriteRenderer.color = buttonShadingColor;
			upArrowButton.buttonPress.locked = true;
		}
		/*
		else if (mode == ShadingMode.QUESTION_BUTTON_SHADING)
		{
			hintButton.spriteRenderer.color = buttonShadingColor;
			upArrowButton.spriteRenderer.color = buttonShadingColor;
			upArrowButton.buttonPress.locked = true;
			
			collider2D.enabled = true;
		}
		*/
	}
	
	public void ClearShading()
	{
		if (shadingMode == ShadingMode.CALCULATOR_SHADING)
		{
			hintButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
			//questionButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
		else if (shadingMode == ShadingMode.BOTTOM_PANEL_SHADING)
		{
			//hintButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
			//questionButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
			upArrowButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
			upArrowButton.buttonPress.locked = false;
		}
		/*
		else if (shadingMode == ShadingMode.QUESTION_BUTTON_SHADING)
		{
			hintButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
			upArrowButton.spriteRenderer.color = new Color32(255, 255, 255, 255);
			upArrowButton.buttonPress.locked = false;
			
			collider2D.enabled = false;
		}
		*/
	
		spriteRenderer.color = new Color32(255, 255, 255, 0);
		
		shadingMode = ShadingMode.NONE;
	}
	
	void OnMouseDown()
	{
		/*
		if (shadingMode == ShadingMode.QUESTION_BUTTON_SHADING)
		{
			//ClearShading();
			questionButton.DeactivateHiddenNumScreen();
		}
		*/
	}
}
