﻿using UnityEngine;
using System.Collections;

public class UndoButton : MonoBehaviour 
{
	GameController gameController;
	
	void Start() 
	{
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();
	}
	
	void OnMouseUp()
	{
		if (!gameController.changindMode)
		{
			gameController.LoadPreviousState();
		}
	}
}
