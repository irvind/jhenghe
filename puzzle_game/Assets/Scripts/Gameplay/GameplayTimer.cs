﻿using UnityEngine;
using System.Collections;

public enum GameplayTimerMode 
{
	NORMAL, WAIT_SEC, COUNT_SEC, WAIT_MIN, COUNT_MIN, STOP, END
}

public class GameplayTimer : MonoBehaviour 
{
	public TextMesh timerText;
	public GameplayTimerMode mode;
	
	public float waitSecTime;
	public float countSecSpeed;
	
	public float waitMinTime;
	public float countMinSpeed;
	
	public float waitStopTime;
	
	private GameController gameController;
	
	private float time;
	
	private int hours;
	private int minutes;
	private int seconds;
	
	private int minutePoints;
	
	public int Minutes
	{
		get
		{
			return minutes;
		}
	}
	
	public int Seconds
	{
		get
		{
			return seconds;
		}
	}

	void Start() 
	{
		mode = GameplayTimerMode.NORMAL;
	
		timerText = GetComponent<TextMesh>();
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();

		time = 0f;
		
		if (PlayerPrefs.HasKey("CurrentTime"))
		{
			SetTime(PlayerPrefs.GetInt("CurrentTime"));
		}
		else
		{
			SetTime(3600);
		}
	}

	void Update() 
	{
		time += Time.deltaTime;
	
		if (mode == GameplayTimerMode.NORMAL)
		{
			if (time > 1)
			{
				DecreaseTime();
				time = 0;
				
				timerText.text = BuildTimeCounterString();
				
				PlayerPrefs.SetInt("CurrentTime", GetTotalTime());
			}
		}
		else if (mode == GameplayTimerMode.WAIT_SEC)
		{
			if (time > waitSecTime)
			{
				mode = GameplayTimerMode.COUNT_SEC;
				time = 0f;
				
				Debug.Log("Count sec");
				
				if (GameSettings.soundEnabled)
				{
					audio.Play();
				}
			}
		}
		else if (mode == GameplayTimerMode.COUNT_SEC)
		{
			if (time > (1 / countSecSpeed))
			{
				time = 0f;
				
				if (!DecreaseSeconds())
				{
					gameController.AddScore(1);
					time = 0f;
					
					if (GameSettings.soundEnabled)
					{
						audio.Stop();
					}
					
					if (GetMinutes() != 0)
					{
						mode = GameplayTimerMode.WAIT_MIN;
						minutePoints = 1 + GetMinutes() / 10;
						
						Debug.Log("Wait min");
					}
					else
					{
						mode = GameplayTimerMode.STOP;
						
						Debug.Log("Stop");
					}
				}
			}
		}
		else if (mode == GameplayTimerMode.WAIT_MIN)
		{
			if (time > waitMinTime)
			{
				mode = GameplayTimerMode.COUNT_MIN;
				time = 0f;
				
				Debug.Log("Count min");
				
				if (GameSettings.soundEnabled)
				{
					audio.Play();
				}
			}
		}
		else if (mode == GameplayTimerMode.COUNT_MIN)
		{
			if (time > (1 / countMinSpeed))
			{
				time = 0f;
				
				if (!DecreaseMinutes())
				{
					mode = GameplayTimerMode.STOP;
					
					Debug.Log("Stop");
					
					if (GameSettings.soundEnabled)
					{
						audio.Stop();
					}
				}
				else
				{
					gameController.AddScore(minutePoints);
				}
			}
		}
		else if (mode == GameplayTimerMode.STOP)
		{
			if (time > waitStopTime)
			{
				mode = GameplayTimerMode.END;
				
				Debug.Log("End");
			}
		}
	}
	
	public int GetCurrentPoints()
	{
		int points = 0;
		
		if (seconds > 0)
		{
			points++;
		}
		
		if (minutes > 0)
		{
			points = points + (1 + GetMinutes() / 10) * minutes;
		}
		
		return points;
	}
	
	public void Finish()
	{
		if (GetSeconds() != 0)
		{
			mode = GameplayTimerMode.WAIT_SEC;
		}
		else
		{
			if (GetMinutes() != 0)
			{
				mode = GameplayTimerMode.WAIT_MIN;
			}
			else
			{
				mode = GameplayTimerMode.STOP;
			}
		}
		
		time = 0f;
	}

	private string BuildTimeCounterString()
	{
		string hoursStr = hours.ToString().PadLeft(2, '0');
		string minutesStr = minutes.ToString().PadLeft(2, '0');
		string secondsStr = seconds.ToString().PadLeft(2, '0');
		
		string resultStr = string.Format("{0}:{1}:{2}", hoursStr, minutesStr, secondsStr);
		
		return resultStr;
	}
	
	public void SetTime(int time)
	{
		hours = time / 3600;
		time %= 3600;
		
		minutes = time / 60;
		time %= 60;
		
		seconds = time;
		
		timerText.text = BuildTimeCounterString();
	}
	
	private void DecreaseTime()
	{
		if ((hours == 0) && (minutes == 0) && (seconds == 0))
		{
			return;
		}
	
		if (seconds != 0)
		{
			seconds--;
		}
		else
		{
			if (minutes != 0)
			{
				minutes--;
				seconds = 59;
			}
			else
			{
				hours--;
				minutes = 59;
				seconds = 59;
			}
		}
	}
	
	private bool DecreaseSeconds()
	{
		if (seconds != 0)
		{
			seconds--;
			timerText.text = BuildTimeCounterString();
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private bool DecreaseMinutes()
	{
		if (minutes != 0)
		{
			minutes--;
			timerText.text = BuildTimeCounterString();
			
			return true;
		}
		else
		{
			return false;
		}
	}
	
	private int GetSeconds()
	{
		string sec = timerText.text.Substring(timerText.text.Length - 2);
		
		return int.Parse(sec);
	}
	
	private int GetMinutes()
	{
		string min = timerText.text.Substring(timerText.text.Length - 5, 2);
		
		return int.Parse(min);
	}
	
	private int GetTotalTime()
	{
		return seconds + minutes * 60 + hours * 3600;
	}
}