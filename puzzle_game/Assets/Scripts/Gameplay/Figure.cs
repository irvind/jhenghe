﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class Figure : MonoBehaviour 
{
	public Transform hiddenNumContainerTransform;
	public GameObject hiddenNumPrefab;

	public float animationLength;
	public int index;
	
	private GameController gameController;
	private SpriteRenderer spriteRenderer;
	public FigureType type;

	private float[][] hiddenNumPositions;
	
	private int[] suggestions;
	//private bool unset;
	
	private bool opened;
	private bool initialized;
	private bool inAnimation;
	private bool isNumsHidden;
	
	private Sprite darkSprite;
	private Sprite orangeSprite;

	private float time;
	private bool stateSetFlag;
	private int[] futureSuggestions;

	private int[] delayedSuggestions;
	private bool delayedIsInstant;
	private bool delayedStayUnset;
	
	public FigureType Type
	{
		get
		{
			return type;
		}
	}

	public int[] Suggestions
	{
		get
		{
			return suggestions;
		}
	}

	public bool InAnimation
	{
		get
		{
			return inAnimation;
		}
	}
	
	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		hiddenNumContainerTransform = transform.FindChild("hidden_num_container");

		type = FigureType.NONE;
		suggestions = new int[0];
		//unset = false;
		
		opened = false;
		inAnimation = false;
		isNumsHidden = false;
		index = -1;

		delayedSuggestions = null;
		
		BuildHiddenNumPositions();

		initialized = false;
	}

	void OnMouseDown()
	{
		if (opened)
		{
			return;
		}
		
		if (gameController.OnFigureClick(this))
		{
			SoundManager.instance.PlaySymbolClickSound();
		}
	}
	
	void Update()
	{
		if (inAnimation)
		{
			time += Time.deltaTime;

			Vector3 fromVec;
			Vector3 toVec;
			float t;

			float halfLength = animationLength / 2;

			if (time < halfLength)
			{
				fromVec = new Vector3(1, 1, 1);
				toVec = new Vector3(0.1f, 0.1f, 0.1f);
				t = time / halfLength;
			}
			else
			{
				fromVec = new Vector3(0.1f, 0.1f, 0.1f);
				toVec = new Vector3(1, 1, 1);

				if (!stateSetFlag)
				{
					SetSuggestionState(futureSuggestions);

					stateSetFlag = true;
				}

				if (time > animationLength)
				{
					t = 1f;
					inAnimation = false;
				}
				else
				{
					t = (time - halfLength) / halfLength;
				}
			}

			transform.localScale = Vector3.Lerp(fromVec, toVec, t);
		}
	}
	
	public void Initialize(FigureType inType, int[] startSug = null, bool isOpened = false)
	{
		if (initialized)
		{
			return;
		}
		
		if ((startSug != null) && (startSug.Length != 1) && isOpened)
		{
			return;
		}
		
		type = inType;
		opened = isOpened;
		
		int typeInt = (int)type;
		darkSprite = gameController.darkFiguresSprites[typeInt];
		orangeSprite = gameController.orangefiguresSprites[typeInt];

		if (startSug == null)
		{
			startSug = new int[0];
		}

		SetSuggestionState(startSug);
		
		initialized = true;
	}
	
	public void ApplySuggestions(int[] inSuggestions, bool instantly = false, bool stayUnset = false)
	{
		if (opened || inSuggestions == null)
		{
			return;
		}

		if (CheckSuggestionEquality(inSuggestions))
		{
			return;
		}

		if (isNumsHidden)
		{
			if ((suggestions.Length == 0) && (inSuggestions.Length > 1) 
			    || ((suggestions.Length > 1) && (inSuggestions.Length == 0)))
			{
				instantly = true;
			}
		}

		if (instantly)
		{
			SetSuggestionState(inSuggestions, stayUnset);
		}
		else
		{
			inAnimation = true;
			stateSetFlag = false;
			time = 0f;

			futureSuggestions = inSuggestions;
		}
	}

	public void SetDelayedSuggestions(int[] inSuggestions, bool instantly = false, bool stayUnset = false)
	{
		string message = "Set delayed suggestion state in suggestions: ";
		for (int i = 0; i < inSuggestions.Length; i++)
		{
			message += inSuggestions[i].ToString() + " ";
		}
		
		Debug.Log(message);
	
		if (CheckSuggestionEquality(inSuggestions))
		{
			Debug.Log("Test 1 failed");
			
			return;
		}

		if (isNumsHidden)
		{
			/*if ((suggestions.Length == 0) && (inSuggestions.Length > 1) 
			    || ((suggestions.Length > 1) && (inSuggestions.Length == 0)))*/
			if (CheckSuggestionsUnset(suggestions) && CheckSuggestionsUnset(inSuggestions))
			{
				Debug.Log("Test 2 failed");
				
				SetSuggestionState(inSuggestions);
				
				return;
			}
		}

		delayedSuggestions = inSuggestions;
		delayedIsInstant = instantly;
		delayedStayUnset = stayUnset;
	}

	public bool ApplyDelayedSuggestions()
	{
		if (delayedSuggestions != null)
		{
			ApplySuggestions(delayedSuggestions, delayedIsInstant, delayedStayUnset);
			delayedSuggestions = null;

			return true;
		}
		else
		{
			return false;
		}
	}

	public void ToogleNums()
	{
		isNumsHidden = !isNumsHidden;

		if (isNumsHidden)
		{
			hiddenNumContainerTransform.gameObject.SetActive(false);

			if ((suggestions.Length > 1) || (suggestions.Length == 1 && suggestions[0] < 0))
			{
				spriteRenderer.sprite = orangeSprite;
				spriteRenderer.color = new Color32(255, 255, 255, 255);
			}
		}
		else
		{
			hiddenNumContainerTransform.gameObject.SetActive(true);

			if ((suggestions.Length > 1) || (suggestions.Length == 1 && suggestions[0] < 0))
			{
				spriteRenderer.sprite = darkSprite;
				spriteRenderer.color = new Color32(85, 85, 85, 255);
			}
		}
	}
	
	public FigureType GetFigureType()
	{
		return type;
	}
	
	private bool CheckSuggestionsUnset(int[] inSuggestions)
	{
		if (!isNumsHidden)
		{
			return false;
		}
		
		if (inSuggestions.Length == 0)
		{
			return true;
		}
		
		if ((inSuggestions.Length == 1) && (inSuggestions[0] < 0))
		{
			return true;
		}
		
		if (inSuggestions.Length > 1)
		{
			return true;
		}
		
		return false;
	}
	
	private void SetSuggestionState(int[] inSuggestions, bool stayUnset = false)
	{
		string message = "Set suggestion state in suggestions: ";
		for (int i = 0; i < inSuggestions.Length; i++)
		{
			message += inSuggestions[i].ToString() + " ";
		}
		
		Debug.Log(message);
	
		ClearHiddenNumbers();

		if (inSuggestions.Length == 0)
		{
			spriteRenderer.sprite = orangeSprite;
			spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
		else if ((inSuggestions.Length == 1) && (inSuggestions[0] >= 0))
		{
			if (!opened)
			{
				spriteRenderer.sprite = gameController.darkNumbersSprites[inSuggestions[0]];
			}
			else
			{
				spriteRenderer.sprite = gameController.orangeNumbersSprites[inSuggestions[0]];
			}

			spriteRenderer.color = new Color32(255, 255, 255, 255);
		}
		else // if (startSug.Length > 1)
		{
			if (!isNumsHidden)
			{
				spriteRenderer.sprite = darkSprite;
				spriteRenderer.color = new Color32(85, 85, 85, 255);
			}
			else
			{
				spriteRenderer.sprite = orangeSprite;
				spriteRenderer.color = new Color32(255, 255, 255, 255);
			}

			SetHiddenNumbers(inSuggestions);
		}
		
		suggestions = inSuggestions;
		//unset = stayUnset;
	}

	private void ClearHiddenNumbers()
	{
		if (hiddenNumContainerTransform.childCount > 0)
		{
			foreach (Transform childTransform in hiddenNumContainerTransform)
			{
				Destroy(childTransform.gameObject);
			}
		}
	}

	private void SetHiddenNumbers(int[] hiddenNumbers)
	{
		if (hiddenNumbers.Length == 0)
		{
			return;
		}
		
		int[] arrClone = (int[])hiddenNumbers.Clone();
		
		/*
		if ((hiddenNumbers.Length == 1) && (hiddenNumbers[0] < 0))
		{
			if (hiddenNumbers[0] == -10)
			{
				hiddenNumbers[0] = 0;
			}
			else
			{
				hiddenNumbers[0] = -hiddenNumbers[0];
			}
		}
		*/
		
		if ((hiddenNumbers.Length == 1) && (hiddenNumbers[0] < 0))
		{
			if (hiddenNumbers[0] == -10)
			{
				arrClone[0] = 0;
			}
			else
			{
				arrClone[0] = -hiddenNumbers[0];
			}
		}
		
		ClearHiddenNumbers();
		
		List<int> numList = new List<int>(/*hiddenNumbers*/arrClone);
		numList.Sort();
		
		float[] positions = hiddenNumPositions[numList.Count - 1];
		
		for (int i = 0; i < numList.Count; i++)
		{
			GameObject obj = (GameObject)Instantiate(hiddenNumPrefab, Vector3.zero, Quaternion.identity);
				
			obj.transform.parent = hiddenNumContainerTransform;
			obj.transform.localPosition = new Vector3(positions[i * 2], positions[i * 2 + 1], 0f);
			obj.transform.localScale = new Vector3(1f, 1f, 1f);
			obj.GetComponent<TextMesh>().text = numList[i].ToString();
		}
	}
	
	private void BuildHiddenNumPositions()
	{
		float width = 0.28f;
		float hwidth = width / 2;
		
		float height = 0.38f;
		float hheight = height / 2;
		
		hiddenNumPositions = new float[10][];
		
		hiddenNumPositions[0] = new float[] {0f, 0f};
		hiddenNumPositions[1] = new float[] {-hwidth, 0f, hwidth, 0f};
		hiddenNumPositions[2] = new float[] {-hwidth, hheight, hwidth, hheight, 0f, -hheight};
		hiddenNumPositions[3] = new float[] {-hwidth, hheight, hwidth, hheight, -hwidth, -hheight, 
			hwidth, -hheight};
		hiddenNumPositions[4] = new float[] {-width, hheight, 0f, hheight, width, hheight, 
			-hwidth, -hheight, hwidth, -hheight};
		hiddenNumPositions[5] = new float[] {-width, hheight, 0f, hheight, width, hheight,
			-width, -hheight, 0f, -hheight, width, -hheight};
		hiddenNumPositions[6] = new float[] {-width - hwidth, hheight, -hwidth, hheight, hwidth, hheight, 
			hwidth + width, hheight, -width, -hheight, 0f, -hheight, width, -hheight};
		hiddenNumPositions[7] = new float[] {-width - hwidth, hheight, -hwidth, hheight, hwidth, hheight, 
			hwidth + width, hheight, -width - hwidth, -hheight, -hwidth, -hheight, hwidth, -hheight, 
			hwidth + width, -hheight};
		hiddenNumPositions[8] = new float[] {-width, height, 0f, height, width, height, -width, 0f, 0f, 0f, 
			width, 0f, -width, -height, 0f, -height, width, -height};
		hiddenNumPositions[9] = new float[] {-width - hwidth, height, -hwidth, height, hwidth, height, 
			hwidth + width, height, -width, 0f, 0f, 0f, width, 0f, -width, -height, 0f, -height, 
			width, -height};
	}

	private bool CheckSuggestionEquality(int[] inSuggestions)
	{
		if (inSuggestions.Length == suggestions.Length)
		{
			bool equal = true;
			for (int i = 0; i < inSuggestions.Length; i++)
			{
				if (inSuggestions[i] != suggestions[i])
				{
					equal = false;
					break;
				}
			}

			return equal;
		}
		else
		{
			return false;
		}
	}
}
