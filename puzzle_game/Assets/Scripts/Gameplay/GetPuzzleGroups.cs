﻿using UnityEngine;
using System.Collections;

public class GetPuzzleGroups : MonoBehaviour 
{
	public Transform[][] groups;

	void Awake() 
	{
		groups = new Transform[5][];

		for (int i = 0; i < 5; i++)
		{
			if (i % 2 == 0)
			{
				groups[i] = new Transform[5];
			}
			else
			{
				groups[i] = new Transform[3];
			}
		}

		for (int i = 0; i < 5; i++)
		{
			string row = string.Format("pc_row{0}", i + 1);

			if (i % 2 == 0)
			{
				for (int j = 0; j < 5; j += 1)
				{
					string group = string.Format("{0}_gr{1}", row, j + 1);
					string name = row + "/" + group;
					groups[i][j] = transform.Find(name);
				}
			}
			else
			{
				for (int j = 0; j < 5; j += 2)
				{
					string group = string.Format("{0}_gr{1}", row, j + 1);
					string name = row + "/" + group;
					groups[i][j / 2] = transform.Find(name);
				}
			}
		}
	}
}
