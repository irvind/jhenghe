﻿using UnityEngine;
using System.Collections;

public class Calculator : MonoBehaviour 
{
	public TextMesh currentText;
	public CalculatorUpArrow upArrow;
	public Transform centerTransform;
	public ScreenShading screenShading;

	public float transitionX;
	public float speed;

	// Активен ли калькулятор (когда он на экране, и он позволяет надимать клавишт). 
	public bool calcActive;
	
	// Текущее значение поля калькулятора
	private double currentValue;

	// Значение в памяти (после выбора операции)
	private double operValue;

	// Задана ли операция
	private bool noOperation;

	// Символ операции.
	private char operation;
	
	private bool waitForNumber;

	// Находитя ли калькулятор в состоянии перемещения
	private bool inTransition;
	
	private bool transIntoActive;
	
	private float activatedY;
	private float deactivatedY;
	
	private float destX;
	private float destY;
	private int destDirection;
	
	private BottomPanel bottomPanel;

	public bool IsEnabled 
	{
		get
		{
			return inTransition || calcActive;
		}
	}

	void Start() 
	{
		bottomPanel = GameObject.FindGameObjectWithTag("BottomPanel").GetComponent<BottomPanel>();

		Reset();

		inTransition = false;
		calcActive = false;

		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		
		Vector3 pos = transform.position;
		//pos.y = cameraBounds.Bottom + 0.38f;
		pos.y = cameraBounds.Bottom - upArrow.transform.localPosition.y + 0.24f;
		transform.position = pos;
		
		deactivatedY = pos.y;
		activatedY = /*-centerTransform.position.y + */Mathf.Abs(centerTransform.localPosition.y);
	}

	void Update() 
	{
		if (inTransition)
		{
			UpdateTransition();
		}
	}

	private void UpdateTransition()
	{
		Vector3 curPos = transform.position;
		curPos.y = Mathf.Lerp(curPos.y, destY, speed * Time.deltaTime);

		if (Mathf.Abs(curPos.y - destY) < 0.02f) 
		{
			curPos.y = destY;
			
			calcActive = transIntoActive;
			inTransition = false;
			
			/*
			if (calcActive)
			{
				upArrow.spriteRenderer.sprite = upArrow.downSprite;
			}
			else
			{
				upArrow.spriteRenderer.sprite = upArrow.upSprite;
			}
			*/
			
			Reset();
		}
		
		transform.position = curPos;
	}

	public void OnCalculatorButtonPress(CalculatorButtonType buttonType, int number = 0)
	{
		if (buttonType == CalculatorButtonType.NUMBER)
		{
			OnNumberButtonPress(number);
		}
		else if (buttonType == CalculatorButtonType.EQUAL)
		{
			OnEqualButtonPress();
		}
		else if (buttonType == CalculatorButtonType.CLEAR)
		{
			OnClearButtonPress();
		}
		else
		{
			if (buttonType == CalculatorButtonType.PLUS)
			{
				OnOperatorButtonPress('+');
			}
			else if (buttonType == CalculatorButtonType.MINUS)
			{
				OnOperatorButtonPress('-');
			}
			else if (buttonType == CalculatorButtonType.MULTIPLY)
			{
				OnOperatorButtonPress('*');
			}
			else // buttonType == CalculatorButtonType.DIVISION
			{
				OnOperatorButtonPress('/');
			}
		}
	}
	
	public void OnUpArrowClick()
	{
		inTransition = true;
		transIntoActive = !calcActive;
		destY = transIntoActive ? activatedY : deactivatedY;
		
		if (transIntoActive) 
		{
			Vector3 pos = bottomPanel.transform.position;
			pos.z = -4.5f;
			bottomPanel.transform.position = pos;
			
			pos = transform.position;
			pos.z = -5f;
			transform.position = pos;
			
			//gameController.DeselectFigure();
			
			screenShading.SetShading(ShadingMode.CALCULATOR_SHADING);
			
			//upArrow.spriteRenderer.sprite = upArrow.downSprite;
		}
		else
		{
			screenShading.ClearShading();
			
			//upArrow.spriteRenderer.sprite = upArrow.upSprite;
		}
	}

	public bool IsInTransition()
	{
		return inTransition;
	}

	private void OnNumberButtonPress(int number)
	{
		if (waitForNumber)
		{
			SetCalculatorValue(number, false);
			waitForNumber = false;
		}
		else
		{
			if (currentValue >= 0)
			{
				SetCalculatorValue(currentValue * 10 + number, false);
			}
			else
			{
				SetCalculatorValue(currentValue * 10 - number, false);
			}
		}
	}

	private void OnOperatorButtonPress(char oper)
	{
		if (noOperation)
		{
			operation = oper;
			operValue = currentValue;
			noOperation = false;
			waitForNumber = true;
		}
		else
		{
			if (waitForNumber)
			{
				operation = oper;
			}
			else 
			{
				// выполнить предыдущую опрерацию
			}
		}
	}

	private void OnEqualButtonPress()
	{
		if (!noOperation)
		{
			if (!waitForNumber)
			{
				ProcessOperation();
			}
		}
		else
		{
			waitForNumber = true;
		}
	}

	private void OnClearButtonPress()
	{
		Reset();
	}

	private void SetCalculatorValue(double value, bool byOperation)
	{
		bool negativeValue = value < 0;
		value = negativeValue ? -value : value;
		
		string valueStr = string.Format("{0:0.##}", value);
		string resultString = "";
		
		int numCount;
		int numBeforePoint;
		//int numAfterPoint;
		
		int pointIndex = valueStr.IndexOf(".");
		if (pointIndex == -1)
		{
			numCount = valueStr.Length;
			numBeforePoint = numCount;
			//numAfterPoint = 0;
		}
		else
		{
			numCount = valueStr.Length - 1;
			numBeforePoint = pointIndex;
		}
		
		if (numBeforePoint > 7)
		{
			if (byOperation)
			{
				resultString = "9999999";
			}
			else
			{
				resultString = currentText.text;
			}
		}
		else if (numCount > 7)
		{
			int t = 7 - numBeforePoint;
			if (t == 1)
			{
				resultString = valueStr.Remove(pointIndex + 2);
			}
			else if (t == 0)
			{
				resultString = valueStr.Remove(pointIndex);
			}
		}
		else
		{
			resultString = valueStr;
		}
		
		if (negativeValue)
		{
			resultString = "-" + resultString;
		}
		
		currentText.text = resultString;
		currentValue = float.Parse(resultString);
	}

	private void Reset() 
	{
		SetCalculatorValue(0, false);

		waitForNumber = true;
		noOperation = true;
	}

	private void ProcessOperation()
	{
		if (operation == '+')
		{
			SetCalculatorValue(operValue + currentValue, true);
		}
		else if (operation == '-')
		{
			SetCalculatorValue(operValue - currentValue, true);
		}
		else if (operation == '*')
		{
			SetCalculatorValue(operValue * currentValue, true);
		}
		else if (operation == '/')
		{
			SetCalculatorValue(operValue / currentValue, true);
		}

		noOperation = true;
		waitForNumber = true;
	}
}
