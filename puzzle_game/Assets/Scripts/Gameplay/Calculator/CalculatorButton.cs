﻿using UnityEngine;
using System.Collections;

public enum CalculatorButtonType
{
	NUMBER, PLUS, MINUS, MULTIPLY, DIVISION, EQUAL, CLEAR
}

public class CalculatorButton : MonoBehaviour 
{
	public CalculatorButtonType buttonType;
	public int number;

	private SpriteRenderer spriteRenderer;
	private Calculator calculator;
	//private GameController gameController;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		calculator = GameObject.FindGameObjectWithTag("Calculator").GetComponent<Calculator>();

		//gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
	}
	
	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();

		if (!calculator.IsInTransition())
		{
			spriteRenderer.color = new Color32(155, 155, 155, 255);
		}
	}
	
	void OnMouseUp()
	{
		spriteRenderer.color = new Color32(255, 255, 255, 255);

		calculator.OnCalculatorButtonPress(buttonType, number);
	}
}
