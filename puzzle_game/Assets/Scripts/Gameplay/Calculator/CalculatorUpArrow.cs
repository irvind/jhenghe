﻿using UnityEngine;
using System.Collections;

public class CalculatorUpArrow : MonoBehaviour 
{
	public Calculator calculator;
	public Sprite upSprite;
	public Sprite downSprite;
	
	public SpriteRenderer spriteRenderer;
	public ButtonPress buttonPress;

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		buttonPress = GetComponent<ButtonPress>();
	}
	
	void OnMouseDown()
	{
		if (buttonPress.locked)
		{
			return;
		}
	
		if (!calculator.IsInTransition())
		{
			calculator.OnUpArrowClick();
			SoundManager.instance.PlayMenuClickSound();
		}
	}
}
