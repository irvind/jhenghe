﻿using UnityEngine;
using System.Collections;

public class ClearNumButton : MonoBehaviour 
{
	public SpriteRenderer spriteRenderer;
	public ScreenShading screenShading;
	
	private GameController gameController;
	private Calculator calculator;

	void Start () 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		calculator = GameObject.FindGameObjectWithTag("Calculator").GetComponent<Calculator>();
	}
	
	void OnMouseDown()
	{
		if (gameController.gameEnding || calculator.calcActive 
		    || (screenShading.ShadingOn && screenShading.ShadingMode != ShadingMode.BOTTOM_PANEL_SHADING))
		{
			return;
		}
		
		SoundManager.instance.PlayMenuClickSound();
		
		spriteRenderer.color = new Color32(155, 155, 155, 255);
	}
	
	void OnMouseUp()
	{
		if (gameController.gameEnding || calculator.calcActive 
		    || (screenShading.ShadingOn && screenShading.ShadingMode != ShadingMode.BOTTOM_PANEL_SHADING))
		{
			return;
		}
		
		spriteRenderer.color = new Color32(255, 255, 255, 255);
		
		gameController.OnClearNumButtonClick();
	}
}
