﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;

public class CancelButton : MonoBehaviour 
{
	public BottomPanel bottomPanel;
	
	private SpriteRenderer spriteRenderer;

	void Start () 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();

		if (bottomPanel.IsActivated() && !bottomPanel.IsInTransition())
		{
			spriteRenderer.color = new Color32(155, 155, 155, 255);
		}
	}

	void OnMouseUp()
	{
		if (bottomPanel.IsActivated() && !bottomPanel.IsInTransition())
		{
			spriteRenderer.color = new Color32(255, 255, 255, 255);

			bottomPanel.Deactivate();
		}
	}
}
