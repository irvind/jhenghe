﻿using UnityEngine;
using System.Collections;

public enum PanelNumberState
{
	NORMAL, ACTIVE, LOCKED
}

public class PanelNumber : MonoBehaviour 
{
	public BottomPanel bottomPanel;
	public int number;

	private PanelNumberState state;

	private SpriteRenderer spriteRenderer;
	private Vector3 sourcePosition;
	private Vector3 clickPosition;

	public PanelNumberState State
	{
		get
		{
			return state;
		}

		set
		{
			state = value;

			if (state == PanelNumberState.NORMAL)
			{
				spriteRenderer.color = new Color32(255, 255, 255, 255);
			}
			else if (state == PanelNumberState.ACTIVE)
			{
				spriteRenderer.color = bottomPanel.selectedColor;
			}
			else if (state == PanelNumberState.LOCKED)
			{
				spriteRenderer.color = bottomPanel.lockedColor;
			}
		}
	}

	void Start() 
	{
		spriteRenderer = GetComponent<SpriteRenderer>();

		State = PanelNumberState.NORMAL;
	}

	void OnMouseDown()
	{
		if (bottomPanel.IsActivated() && !bottomPanel.IsInTransition() && (State != PanelNumberState.LOCKED))
		{
			SoundManager.instance.PlayNumberClickSound();

			sourcePosition = transform.position;
			clickPosition = new Vector3(sourcePosition.x + 0.02f, sourcePosition.y - 0.02f,
				sourcePosition.z);

			transform.position = clickPosition;

			if (State == PanelNumberState.NORMAL)
			{
				State = PanelNumberState.ACTIVE;
			}
			else if (State == PanelNumberState.ACTIVE)
			{
				State = PanelNumberState.NORMAL;
			}
		}
	}

	void OnMouseUp()
	{
		if (bottomPanel.IsActivated() && !bottomPanel.IsInTransition() && (State != PanelNumberState.LOCKED))
		{
			transform.position = sourcePosition;
		}
	}
}
