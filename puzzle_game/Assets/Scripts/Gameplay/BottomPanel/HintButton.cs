﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HintButton : MonoBehaviour 
{
	public ScreenShading screenShading;
	public SpriteRenderer spriteRenderer;
	public TextMesh hintText;
	
	private GameController gameController;
	private Calculator calculator;

	void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();

		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		calculator = GameObject.FindGameObjectWithTag("Calculator").GetComponent<Calculator>();
		
		hintText.text = PlayerPrefs.GetInt("HintCount").ToString();
	}

	void OnMouseDown()
	{
		if (gameController.gameEnding || calculator.calcActive 
		    || (screenShading.ShadingOn && screenShading.ShadingMode != ShadingMode.BOTTOM_PANEL_SHADING))
		{
			return;
		}

		SoundManager.instance.PlayMenuClickSound();

		spriteRenderer.color = new Color32(155, 155, 155, 255);
	}

	void OnMouseUp()
	{
		if (gameController.gameEnding || calculator.calcActive 
		    || (screenShading.ShadingOn && screenShading.ShadingMode != ShadingMode.BOTTOM_PANEL_SHADING))
		{
			return;
		}

		spriteRenderer.color = new Color32(255, 255, 255, 255);

		gameController.OnHintButtonClick(this);
	}
}
