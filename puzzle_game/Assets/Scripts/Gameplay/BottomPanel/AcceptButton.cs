﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AcceptButton : MonoBehaviour
{
	public GameObject panel;
	
	private BottomPanel panelController;
	private GameController gameController;
	private SpriteRenderer spriteRenderer;

	private PanelNumber[] panelNumbers;

	void Start() 
	{
		panelController = panel.GetComponent<BottomPanel>();
		gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
		spriteRenderer = GetComponent<SpriteRenderer>();

		GameObject[] objects = GameObject.FindGameObjectsWithTag("PanelNumber");
		panelNumbers = new PanelNumber[objects.Length];
		
		for (int i = 0; i < objects.Length; i++)
		{
			panelNumbers[i] = objects[i].GetComponent<PanelNumber>();
		}
	}

	void OnMouseDown()
	{
		SoundManager.instance.PlayMenuClickSound();

		if (panelController.IsActivated() && !panelController.IsInTransition())
		{
			spriteRenderer.color = new Color32(155, 155, 155, 255);
		}
	}
	
	void OnMouseUp()
	{
		if (panelController.IsActivated() && !panelController.IsInTransition())
		{
			spriteRenderer.color = new Color32(255, 255, 255, 255);

			List<int> activeNumbers = new List<int>();
			for (int i = 0; i < panelNumbers.Length; i++)
			{
				if (panelNumbers[i].State == PanelNumberState.ACTIVE)
				{
					activeNumbers.Add(panelNumbers[i].number);
				}
			}

			activeNumbers.Sort();

			gameController.OnAcceptButtonClick(activeNumbers.ToArray());
		}
	}
}
