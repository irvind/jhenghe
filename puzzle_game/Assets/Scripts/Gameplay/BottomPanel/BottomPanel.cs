﻿using UnityEngine;
using System.Collections;

public class BottomPanel : MonoBehaviour 
{
	public ScreenShading screenShading;

	public float panelSpeed;
	public float activatedPosY;
	public float deactivatedPosY;

	public Color32 selectedColor;
	public Color32 lockedColor;

	public float startSpeed;
	public float speedChange;
	public float endSpeed;

	private float currentSpeed;
	private int currentDirection;

	public bool activated = false;
	public bool inTransition = false;

	private bool destState;
	private float destY;

	private Calculator calculator;
	private PanelNumber[] panelNumbers;

	void Start() 
	{
		calculator = GameObject.FindGameObjectWithTag("Calculator").GetComponent<Calculator>();

		GameObject[] objects = GameObject.FindGameObjectsWithTag("PanelNumber");
		panelNumbers = new PanelNumber[objects.Length];
		
		for (int i = 0; i < objects.Length; i++)
		{
			panelNumbers[i] = objects[i].GetComponent<PanelNumber>();
		}

		CameraBounds cameraBounds = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraBounds>();
		deactivatedPosY = cameraBounds.Bottom - 2f;

		Vector3 vec = transform.position;
		vec.y = deactivatedPosY;
		transform.position = vec;
	}

	void Update() 
	{
		if (inTransition)
		{
			UpdateTransition();
		}
	}

	private void UpdateTransition()
	{
		Vector3 pos = transform.position;
		pos.y = Mathf.Lerp(pos.y, destY, panelSpeed * Time.deltaTime);
		
		transform.position = pos;
		
		if (Mathf.Abs(pos.y - destY) < 0.05f)
		{
			inTransition = false;
			activated = destState;
			
			if (!activated)
			{
				/*
				for (int i = 0; i < panelNumbers.Length; i++)
				{
					panelNumbers[i].Deactivate();
				}
				*/
			}
		}
	}

	public void Activate()
	{
		inTransition = true;
		destState = true;
		destY = activatedPosY;
		
		Vector3 pos = calculator.transform.position;
		pos.z = -4.5f;
		calculator.transform.position = pos;
		
		pos = transform.position;
		pos.z = -5f;
		transform.position = pos;
		
		screenShading.SetShading(ShadingMode.BOTTOM_PANEL_SHADING);
	}
	
	public void Deactivate(bool clearShading = true)
	{
		inTransition = true;
		destState = false;
		destY = deactivatedPosY;
		
		if (clearShading)
		{
			screenShading.ClearShading();
		}
	}

	public bool IsActivated()
	{
		return activated;
	}

	public bool IsInTransition()
	{
		return inTransition;
	}
}
